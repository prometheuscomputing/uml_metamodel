= ModelMetadata

== Description:

A ruby implementation of the UML Metamodel. 

While this project is not intended to be strictly adherent to the OMG UML metamodel specification, it does provide the most of the UML Metamodel components used by UML Class Diagrams.

== Changes from model_metadata/metamodel API
* Added Project wrapper that replaces Metainfo and Change classes
* namespace is now UmlMetamodel instead of Metamodel
* Creating an association using two properties now sets the type of each property to the owner of the opposite property end automatically.
* UmlMetamodel::Property type is now set via Property#type= instead of Property#add_type
* EnumerationLiteral.new now only has one required parameter, the 'name' of the literal.
  * The parent of the literal is now set up the same as other elements, by using an 'add' method on the parent with the literal as an argument: 
  * Enumeration#add_literal(literal) or alternatively Enumeration#add_literal_name(literal_name)
* Removed EnumerationLiteral#type property
* Relational DB integration methods are no longer loaded by default, and must be required (uml_metamodel/relational_extensions.rb)
* Tag.new now only has one required parameter, the name of the tag
* Association.new now has only one parameter, options. properties are specified as the value of the :properties key in the hash
* Element#apply_stereotype now accepts a Stereotype as a parameter, not an AppliedStereotype
* AppliedTag.new now has only one parameter, options. applied_stereotype, tag, and value are now specified as keys in that hash.
  * The applied_stereotype of the AppliedTag is now set up the same as other elements, by using an 'add' method on the applied_stereotype:
  * AppliedStereotype#add_applied_tag(applied_tag)
* Property#is_attribute is now Property#attribute? to be consistent with other property methods
* Property#composition? is now Property#composite? to match aggregation specification
* Removed Classifier#base_primitive? since it needed to be rewritten anyway

== Known Issues
* applied_stereotypes are only supported on classifiers and packages when outputting to DSL (should be all elements)
* element imports are not supported (only package imports are)