# rubocop:disable Metrics/BlockLength, Lint/Void
# Disable Lint/Void cop since it doesn't understand RSpec's "should X" syntax
require 'spec_helper'

describe 'metamodel functionality' do
  before :each do
    # Create project wrapper
    @car_example = UmlMetamodel::Project.new('CarExample', :version => '1.2.3', :generated_at => Time.now, :id => '12345.123', :documentation => 'CarExample Project')
    # Create Application model
    @application = UmlMetamodel::Model.new('Application', :id => 1, :documentation => 'CarExample Application')
    @car_example.add_content(@application)
    # Set up packages
    @automotive = UmlMetamodel::Package.new('Automotive', :id => 2, :documentation => 'Automotive package')
    @application.add_import(@automotive)
    @people = UmlMetamodel::Package.new('People')
    @application.add_import(@people)
    @clown_package = UmlMetamodel::Package.new('Clown')
    @people.add_content(@clown_package)
    # Set up classes
    # Set up Person
    @person = UmlMetamodel::Class.new('Person')
    # NOTE: is a PersonName in actual car_example model 2.3.0
    @name = UmlMetamodel::Property.new('name')
    @name.type = UmlMetamodel::PRIMITIVES[:string]
    @person.add_property(@name)
    @vehicles = UmlMetamodel::Property.new('vehicles', :is_navigable => false)
    @person.add_property(@vehicles)
    @people.add_content(@person)
    
    # Set up Clown class
    @clown_class = UmlMetamodel::Class.new('Clown', :id => 12)
    @clown_package.add_content(@clown_class)
    
    # Set up VehicleMaker enumeration (simplified from car_example)
    @vehicle_maker = UmlMetamodel::Enumeration.new('VehicleMaker', :id => '_%%^')
    @vehicle_maker.add_literal_name 'Ford'
    @vehicle_maker.add_literal_name 'Dodge'
    @vehicle_maker.add_literal_name 'Honda'
    @vehicle_maker.add_literal_name 'Volvo'
    @unknown_maker = @vehicle_maker.add_literal_name 'Unknown'
    @unknown_maker.id = 3
    @unknown_maker.documentation = 'Unknown enumeration_literal'
    @vehicle_maker.add_literal_name 'Home Made'
    @vehicle_maker.add_literal_name 'Other'
    @automotive.add_content(@vehicle_maker)
    
    # Set up Vehicle
    @vehicle = UmlMetamodel::Class.new('Vehicle', :id => 4, :documentation => 'Vehicle class')
    @vehicle.is_abstract = true
    @make = UmlMetamodel::Property.new('make', :id => 5, :documentation => 'make property')
    @make.type = @vehicle_maker
    # @make.default_value = @unknown_maker
    @vehicle.add_property(@make)
    @model = UmlMetamodel::Property.new('vehicle_model')
    @model.type = UmlMetamodel::PRIMITIVES[:string]
    @vehicle.add_property(@model)
    @owners = UmlMetamodel::Property.new('owners', :id => 'the_owners')
    @vehicle.add_property(@owners)
    @automotive.add_content(@vehicle)
    
    # Set up Car
    @car = UmlMetamodel::Class.new('Car', :id => 'the_car')
    @vehicle.add_child(@car)
    @automotive.add_content(@car)
    
    # Set up Person -> Vehicle Ownership association
    @ownership = UmlMetamodel::Class.new('Ownership')
    @percent_ownership = UmlMetamodel::Property.new('percent_ownership')
    @percent_ownership.type = UmlMetamodel::PRIMITIVES[:float]
    @percent_ownership.default_value = 100.0
    @ownership.add_property(@percent_ownership)
    @people.add_content(@ownership)
    # NOTE: in MD, name is defined to be the name of the association class. This is not enforced here
    @ownership_association = UmlMetamodel::Association.new(:properties => [@owners, @vehicles], :name => 'ownership_association', :id => 6, :documentation => 'ownership association')
    @ownership_association.association_class = @ownership
    @people.add_content(@ownership_association)
    
    # Set up Warrantied interface
    @warrantied = UmlMetamodel::Interface.new('Warrantied')
    @automotive.add_content(@warrantied)
    @warranty_void = UmlMetamodel::Property.new('warranty_void', :type => UmlMetamodel::PRIMITIVES[:boolean])
    @warrantied.add_property(@warranty_void)
    @warrantied.add_implementor(@vehicle)
    
    # Create options stereotype
    @options = UmlMetamodel::Stereotype.new('options', :id => 7, :documentation => 'options stereotype')
    @application.add_content(@options)
    # Create project_name tag
    @project_name = UmlMetamodel::Tag.new('project_name', :type => UmlMetamodel::PRIMITIVES[:string], :id => 8, :documentation => 'project_name tag')
    @options.add_tag(@project_name)
    # Create applied stereotype on Application
    @applied_options = @application.apply_stereotype(@options)
    @applied_options.should be_a(UmlMetamodel::AppliedStereotype)
    @applied_options.id = 9
    @applied_options.documentation = 'options->application applied_stereotype'
    @applied_options.instance_of.should == @options
    @options.instances.length.should == 1
    # Create applied tag value on <<options>> steretype on Application
    @application.set_tag_value('options', 'project_name', 'CarExample')
    @applied_project_name = @applied_options.applied_tags.find{|at| at.name == 'project_name'}
    @applied_project_name.should be_a(UmlMetamodel::AppliedTag)
    @applied_project_name.id = 10
    @applied_project_name.documentation = 'project_name->options->application applied_tag'
    @application.get_tag_value('options', 'project_name').should == 'CarExample'
    # Create applied stereotype on a vehicle make property
    @applied_options_to_property = @make.apply_stereotype(@options)
    @applied_options_to_property.should be_a(UmlMetamodel::AppliedStereotype)
    @applied_options_to_property.id = 11
    
    # Set up test datatype
    @my_datatype = UmlMetamodel::Datatype.new('MyDatatype')
    @automotive.add_content @my_datatype
  end

  it 'should prevent invalid inheritance relationships from being created' do
    lambda{ @warrantied.add_child @vehicle }.should raise_error(/classifiers .* inherit from a different type/i)
    lambda{ @vehicle.add_child @warrantied }.should raise_error(/classifiers .* inherit from a different type/i)
    lambda{ @warrantied.add_parent @vehicle }.should raise_error(/classifiers .* inherit from a different type/i)
    lambda{ @vehicle.add_parent @warrantied }.should raise_error(/classifiers .* inherit from a different type/i)
  end
  
  it 'should be able to return specific contents via convenience methods' do
    # Test association_classes
    @people.association_classes.should =~ [@ownership]
    @automotive.association_classes.should == []
    @car_example.association_classes.should == []
    @car_example.association_classes(:include_imports => true).should == []
    @application.association_classes(:recurse => true).should == []
    @application.association_classes(:recurse => true, :include_imports => true).should == [@ownership]
    @car_example.association_classes(:recurse => true, :include_imports => true).should =~ [@ownership]
    # test Classifier#associations
    @vehicle.associations.should =~ [@ownership_association]
  end
  
  it 'should be able to build a metamodel using the metamodel API' do
    # Test metamodel
    @application.imports.should =~ [@people, @automotive]
    @people.packages.should =~ [@clown_package]
    @automotive.packages.should == []
    @people.classes.should =~ [@person, @ownership]
    @vehicle.properties.should =~ [@make, @model, @owners]
    @vehicle.children.should =~ [@car]
    @car.parents.should =~ [@vehicle]
  end
  
  it 'should automatically set up association property types on assignment' do
    @owners.type.should == @person
    @vehicles.type.should == @vehicle
  end
  
  it 'should be able to manipulate metamodel component associations' do
    # Test one-to-one (Association (association_class_for) <---> (association_class) Class )
    assoc1 = UmlMetamodel::Association.new(:name => 'assoc1')
    assoc2 = UmlMetamodel::Association.new(:name => 'assoc2')
    assoc_class1 = UmlMetamodel::Class.new('AC1')
    assoc_class2 = UmlMetamodel::Class.new('AC2')
    # Set up inital assocs
    assoc1.association_class = assoc_class1
    assoc2.association_class = assoc_class2
    # Reassign assoc1
    assoc1.association_class = assoc_class2
    # Test
    assoc1.association_class.should == assoc_class2
    assoc_class2.association_class_for.should == assoc1
    assoc_class1.association_class_for.should == nil
    assoc2.association_class.should == nil
    
    # Test many-to-one (Classifier (owner) <---> (properties) Property)
    prop1 = UmlMetamodel::Property.new('a')
    prop2 = UmlMetamodel::Property.new('b')
    prop3 = UmlMetamodel::Property.new('c')
    klass1 = UmlMetamodel::Class.new('A')
    klass2 = UmlMetamodel::Class.new('B')
    # Set up initial assocs
    klass1.add_property prop1
    klass1.add_property prop2
    klass1.add_property prop3
    # Reassign prop1
    klass2.add_property prop1
    # Test
    klass1.properties.should =~ [prop2, prop3]
    klass2.properties.should =~ [prop1]
    prop1.owner.should == klass2
    prop2.owner.should == klass1
    prop3.owner.should == klass1
    # Remove properties from klass1
    klass1.remove_property(prop2)
    prop3.owner = klass2
    # Retest
    klass1.properties.should == []
    klass2.properties.should =~ [prop1, prop3]
    prop1.owner.should == klass2
    prop2.owner.should == nil
    prop3.owner.should == klass2
    
    # Test many-to-many (Classifier (parents) <---> (children) Classifier)
    klass1 = UmlMetamodel::Class.new('A')
    klass2 = UmlMetamodel::Class.new('B')
    klass3 = UmlMetamodel::Class.new('C')
    klass4 = UmlMetamodel::Class.new('D')
    klass5 = UmlMetamodel::Class.new('E')
    klass6 = UmlMetamodel::Class.new('F')
    # Set up initial assocs
    klass1.parents = [klass2, klass3]
    klass4.add_parent klass1
    klass1.add_child klass5
    klass5.children = [klass6]
    # Remove klass5 from klass1
    klass1.remove_child klass5
    # Test
    klass1.parents.should =~ [klass2, klass3]
    klass1.children.should =~ [klass4]
    klass2.parents.should == []
    klass2.children.should == [klass1]
    klass3.parents.should == []
    klass3.children.should == [klass1]
    klass4.parents.should == [klass1]
    klass4.children.should == []
    klass5.parents.should == []
    klass5.children.should == [klass6]
    klass6.parents.should == [klass5]
    klass6.children.should == []
  end
  
  it 'should be able to find an element using its fully qualified name' do
    # #find first searches by id and then by qualified name.
    # Test finding class
    UmlMetamodel.find('Automotive::Car', @car_example).should == @car
    UmlMetamodel.find('Automotive::Car', @application).should == @car
    UmlMetamodel.find('Automotive::Car', @application, :search_imports => false).should == nil
    UmlMetamodel.find('Automotive::Car', @application, :type => UmlMetamodel::Class).should == @car
    UmlMetamodel.find('Automotive::Car', @application, :type => UmlMetamodel::Package).should == nil
    # Test searching containing contexts
    UmlMetamodel.find('Automotive::Car', @automotive).should == nil
    UmlMetamodel.find('Automotive::Car', @car).should == nil
    UmlMetamodel.find('Automotive::Car', @vehicle).should == nil
    UmlMetamodel.find('Automotive::Car', @make).should == nil
    UmlMetamodel.find('Automotive::Car', @automotive, :search_containing => true).should == @car
    UmlMetamodel.find('Automotive::Car', @car, :search_containing => true).should == @car
    UmlMetamodel.find('Automotive::Car', @vehicle, :search_containing => true).should == @car
    UmlMetamodel.find('Automotive::Car', @make, :search_containing => true).should == @car
    
    UmlMetamodel.find_in_elements('Automotive::Car', [@automotive]).should == @car
    UmlMetamodel.find_in_elements('Automotive::Car', @application.imports).should == @car
    UmlMetamodel.find('Car', @automotive).should == @car
    UmlMetamodel.find_in_elements('Car', @automotive.contents).should == @car
    UmlMetamodel.find_in_elements('Car', [@car]).should == @car
    # UmlMetamodel.find('People::Clown::Clown', @application).should == @clown_class
    # finding property
    UmlMetamodel.find('Automotive::Vehicle::owners', @car_example).should == @owners
    UmlMetamodel.find('Automotive::Vehicle::owners', @application).should == @owners
    # finding association
    UmlMetamodel.find('People::ownership_association', @car_example).should == @ownership_association
    UmlMetamodel.find('People::ownership_association', @application).should == @ownership_association
    # finding enumeration
    UmlMetamodel.find('Automotive::VehicleMaker', @car_example).should == @vehicle_maker
    UmlMetamodel.find('Automotive::VehicleMaker', @application).should == @vehicle_maker
    # finding enumeration literal
    UmlMetamodel.find('Automotive::VehicleMaker::Unknown', @car_example).should == @unknown_maker
    UmlMetamodel.find('Automotive::VehicleMaker::Unknown', @application).should == @unknown_maker
    @unknown_maker.qualified_name.should == 'Automotive::VehicleMaker::Unknown'
    # finding stereotype
    # finding tag
    # finding applied stereotype/tags
    
    # finding UML primitives (without context)
    # note that the value of @id for UML::String is 'String'
    UmlMetamodel.find('String', []).should == UmlMetamodel::PRIMITIVES[:string]
    UmlMetamodel.find_by_qualified_name('UML::String', []).should == UmlMetamodel::PRIMITIVES[:string]
    UmlMetamodel.find_by_qualified_name('String', []).should == nil
    # Test require_uml_namespace option
    UmlMetamodel.find('String', [], :require_uml_namespace => false).should == UmlMetamodel::PRIMITIVES[:string]
    UmlMetamodel.find('UML::String', [], :require_uml_namespace => false).should == UmlMetamodel::PRIMITIVES[:string]
    # Test search_uml option
    UmlMetamodel.find('String', [], :search_uml => false).should == nil
    UmlMetamodel.find('UML::String', [], :search_uml => false).should == nil
    # finding UML primitives (with context)
    UmlMetamodel.find('UML::String', @car_example).should == UmlMetamodel::PRIMITIVES[:string]
    UmlMetamodel.find_by_qualified_name('String', @car_example).should == nil
    UmlMetamodel.find('String', @car_example).should == UmlMetamodel::PRIMITIVES[:string]
    UmlMetamodel.find('String', @car_example, :require_uml_namespace => false).should == UmlMetamodel::PRIMITIVES[:string]
  end
  
  it 'should be able to find elements using a relatively qualified name via the #find method' do
    @car_example.find('Automotive::Car').should == @car
    @car_example.find('Car').should == nil
    @automotive.find('Automotive::Car').should == nil
    @automotive.find('Car').should == @car
    @car_example.find('Automotive::Vehicle::vehicle_model').should == @model
    @vehicle.find('vehicle_model').should == @model
  end
  
  it 'should not find invalid or non-existant qualified names' do
    UmlMetamodel.find('XML Schema::XML Schema', @application).should == nil
    UmlMetamodel.find('XML Schema::XML Schema::', @application).should == nil
  end
  
  it 'should be able to find an element using its id' do
    # Test finding class
    UmlMetamodel.find_by_id('the_car', @car_example).should == @car
    UmlMetamodel.find_by_id('the_car', @application).should == @car
    UmlMetamodel.find_by_id('the_car', @application, :search_imports => false).should == nil
    # UmlMetamodel.find_by_id('the_car', @application, :type => UmlMetamodel::Class).should == @car
    # UmlMetamodel.find_by_id('the_car', @application, :type => UmlMetamodel::Package).should == nil
    # Test searching containing contexts
    UmlMetamodel.find_by_id('the_car', @automotive).should == @car
    UmlMetamodel.find_by_id('the_car', @car).should == nil
    UmlMetamodel.find_by_id('the_car', @vehicle).should == nil
    UmlMetamodel.find_by_id('the_car', @make).should == nil
    UmlMetamodel.find_by_id('the_car', @automotive, :search_containing => true).should == @car
    UmlMetamodel.find_by_id('the_car', @car, :search_containing => true).should == @car
    UmlMetamodel.find_by_id('the_car', @vehicle, :search_containing => true).should == @car
    UmlMetamodel.find_by_id('the_car', @make, :search_containing => true).should == @car
    
    UmlMetamodel.find_by_id_in_elements('the_car', [@automotive]).should == @car
    UmlMetamodel.find_by_id_in_elements('the_car', @application.imports).should == @car
    UmlMetamodel.find_by_id('the_car', @automotive).should == @car
    UmlMetamodel.find_by_id_in_elements('the_car', @automotive.contents).should == @car
    UmlMetamodel.find_by_id_in_elements('the_car', [@car]).should == @car
    # finding property
    UmlMetamodel.find_by_id('the_owners', @car_example).should == @owners
    UmlMetamodel.find_by_id('the_owners', @application).should == @owners
    # finding association
    UmlMetamodel.find_by_id('6', @car_example).should == @ownership_association
    UmlMetamodel.find_by_id('6', @application).should == @ownership_association
    # finding enumeration
    UmlMetamodel.find_by_id('_%%^', @car_example).should == @vehicle_maker
    UmlMetamodel.find_by_id('_%%^', @application).should == @vehicle_maker
    # finding enumeration literal
    UmlMetamodel.find_by_id('3', @car_example).should == @unknown_maker
    UmlMetamodel.find_by_id('3', @application).should == @unknown_maker
    @unknown_maker.id.should == '3'
    # finding stereotype
    # finding tag
    # finding applied stereotype/tags
    
    # finding UML primitives (without context)
    UmlMetamodel.find_by_id('String', []).should == UmlMetamodel::PRIMITIVES[:string]
    # Test search_uml option
    UmlMetamodel.find_by_id('String', [], :search_uml => false).should == nil
    # finding UML primitives (with context)
    UmlMetamodel.find_by_id('String', @car_example).should == UmlMetamodel::PRIMITIVES[:string]
  end
  
  context 'UmlMetamodel serialization' do
    it 'should be able to serialize and deserialize the metamodel objects via a Marshal string' do
      m = @car_example.to_marshal
      @deserialized_car_example = UmlMetamodel.from_marshal(m)
    end
    
    it 'should be able to serialize and deserialize the metamodel objects via a Marshal file' do
      marshal_file = File.join(TMP_DIR, 'marshalled_car_example')
      m = @car_example.to_marshal_file(marshal_file)
      @deserialized_car_example = UmlMetamodel.from_marshal_file(marshal_file)
      FileUtils.rm(marshal_file)
    end
    
    it 'should be able to serialize and deserialize the metamodel objects via a DSL string' do
      d = @car_example.to_dsl
      @deserialized_car_example = UmlMetamodel.from_dsl(d)
      @deserialized_car_example.contents.length.should == 3
    end
    
    it 'should be able to serialize and deserialize the metamodel objects via a DSL file' do
      dsl_file = File.join(TMP_DIR, 'car_example_dsl_partial.rb')
      d = @car_example.to_dsl_file(dsl_file)
      @deserialized_car_example = UmlMetamodel.from_dsl_file(dsl_file)
      @deserialized_car_example.contents.length.should == 3
      FileUtils.rm(dsl_file)
    end
    
    after :each do
      # Test deserialized car_example package
      @deserialized_car_example.id.should == '12345.123'
      @deserialized_car_example.documentation.should == 'CarExample Project'
      @deserialized_car_example.contents.length.should == 3
      @application = @deserialized_car_example.contents.first
      @application.id.should == '1'
      @application.documentation.should == "CarExample Application"
      @application.imports.length.should == 2
      @application.imports.collect{|p| p.name }.should =~ ['People', 'Automotive']
      people = @application.imports.find{|p|p.name == 'People'}
      automotive = @application.imports.find{|p|p.name == 'Automotive'}
      automotive.id.should == '2'
      automotive.documentation.should == "Automotive package"
      people.packages.length.should == 1
      clown = people.packages.first
      clown.name.should == 'Clown'
      automotive.packages.should == []
      people.classes.length.should == 2
      person = people.classes.find{|c|c.name == 'Person'}
      ownership = people.classes.find{|c|c.name == 'Ownership'}
      vehicle = automotive.classes.find{|c|c.name == 'Vehicle'}
      vehicle.id.should == '4'
      vehicle.documentation.should == 'Vehicle class'
      vehicle.is_abstract.should == true
      car = automotive.classes.find{|c|c.name == 'Car'}
      vehicle.properties.collect{|p|p.name}.should =~ ['make', 'vehicle_model', 'owners']
      vehicle.parents.should == []
      vehicle.children.should =~ [car]
      car.parents.should =~ [vehicle]
      car.children.should == []
      # Check enumeration property and type
      vehicle_make = vehicle.properties.find{|p|p.name == 'make'}
      vehicle_make.id.should == '5'
      vehicle_make.documentation.should == 'make property'
      vehicle_maker = vehicle_make.type
      vehicle_maker.should be_a(UmlMetamodel::Enumeration)
      unknown_maker = vehicle_maker.literals.find{|l|l.name == 'Unknown'}
      unknown_maker.should be_a(UmlMetamodel::EnumerationLiteral)
      unknown_maker.id.should == '3'
      unknown_maker.documentation.should == 'Unknown enumeration_literal'
      # Check association properties
      vehicle_owners = vehicle.properties.find{|prop|prop.name == 'owners'}
      person_vehicles = person.properties.find{|prop|prop.name == 'vehicles'}
      person_vehicles.is_navigable.should be false
      ownership_association = people.associations.find{|assoc|assoc.name == 'ownership_association'}
      ownership_association.id.should == '6'
      ownership_association.documentation.should == 'ownership association'
      ownership_association.properties.should =~ [vehicle_owners, person_vehicles]
      ownership_association.association_class.should == ownership
      vehicle_owners.association.should == ownership_association
      person_vehicles.association.should == ownership_association
      # Check interface relationship
      warrantied = automotive.interfaces.find{|i|i.name == 'Warrantied'}
      warrantied.implementors.should include(vehicle)
      # Check stereotype, tag, applied_stereotype, applied_tag
      applied_options = @application.get_stereotype('options')
      applied_options.id.should == '9'
      applied_options.documentation.should == 'options->application applied_stereotype'
      applied_options.should be_a(UmlMetamodel::AppliedStereotype)
      options = applied_options.instance_of
      options.id.should == '7'
      options.documentation.should == 'options stereotype'
      options.should be_a(UmlMetamodel::Stereotype)
      options.name.should == 'options'
      options.tags.length.should == 1
      project_name = options.tags.first
      project_name.id.should == '8'
      project_name.documentation.should == 'project_name tag'
      project_name.should be_a(UmlMetamodel::Tag)
      project_name.name.should == 'project_name'
      project_name.instances.length.should == 1
      applied_project_name = project_name.instances.first
      applied_project_name.id.should == '10'
      applied_project_name.documentation.should == 'project_name->options->application applied_tag'
      applied_project_name.name.should == 'project_name'
      applied_options.applied_tags.should == [applied_project_name]
      # Check stereotype applied to property
      applied_options_to_property = vehicle_make.get_stereotype('options')
      applied_options_to_property.id.should == '11'
    end
  end
end

describe 'DSL functionality and safety' do
  it 'should only allow access to exposed DSL methods' do
    UmlMetamodel::DSL.parse('klass "Foo" do; end').first.should be_a(UmlMetamodel::Class)
    lambda{ UmlMetamodel::DSL.parse('classifier UmlMetamodel::Class, "Foo" do; end')}.should raise_error(NoMethodError, /classifier/)
    UmlMetamodel::DSL.parse('package "Foo" do; klass "Bar" do; end; end').first.should be_a(UmlMetamodel::Package)
    lambda{ UmlMetamodel::DSL.parse('package "Foo" do; classifier UmlMetamodel::Class, "Bar" do; end; end')}.should raise_error(NoMethodError, /classifier/)
    lambda{ UmlMetamodel::DSL.parse('self.set_deferred_reference_properties')}.should raise_error(NoMethodError, /set_deferred_reference_properties/)
    lambda{ UmlMetamodel::DSL.parse('package "Foo" do; self.set_deferred_reference_properties; end')}.should raise_error(NoMethodError, /set_deferred_reference_properties/)
  end
  
  it 'should be able to convert a DSL project representation to a marshal representation and back again' do
    # Deserialize from DSL
    car_example_dsl_file = File.join(TEST_DATA, 'CarExampleApplication.rb')
    car_example = UmlMetamodel.from_dsl_file(car_example_dsl_file, :suppress_warnings => false)
    
    # Reserialize to Marshal
    marshal_file = File.join(TMP_DIR, 'car_example.marshal')
    car_example.to_marshal_file(marshal_file)
    # Re-deserialize from Marshal
    redeserialized_car_example = UmlMetamodel.from_marshal_file(marshal_file)
    FileUtils.rm(marshal_file)
    
    # Serialize to DSL
    dsl_file = File.join(TMP_DIR, 'car_example_full.rb')
    car_example.to_dsl_file(dsl_file)
    # Ensure serialization is the same as input
    File.read(dsl_file).should == File.read(car_example_dsl_file)
  end
end

describe 'CarExample Metamodel' do
  before(:all) do
    car_example_dsl_file = File.join(TEST_DATA, 'CarExampleApplication.rb')
    # Deserialize from Marshal
    @car_example = UmlMetamodel.from_dsl_file(car_example_dsl_file, :suppress_warnings => false)
  end
  
  it 'should accurately represent People::Person::name' do
    name = UmlMetamodel.find('People::Person::name', @car_example)
    name.lower.should == 0
    name.upper.should == 1
    name.multiplicity_string.should == '0..1'
    name.multiplicity.should == (0..1)
    name.attribute?.should == true
    name.association?.should == false
    name.singular?.should == true
    name.multiple?.should == false
    name.aggregation.should == :none
    name.composite?.should == false
    name.shared?.should == false
    name.unidirectional?.should == true
    name.opposite.should == nil
    name.is_navigable.should == true
    name.is_unique.should == true
    name.is_derived.should == false
    name.is_ordered.should == false
    name.is_globally_unique.should == false
    name.owner.should == UmlMetamodel.find('People::Person', @car_example)
    name.type.should == UmlMetamodel.find('People::PersonName', @car_example)
    name.type.base_primitive.should == UmlMetamodel::PRIMITIVES[:string]
    name.association.should == nil
    name.default_value.should == nil
  end
  
  # Test enumeration property
  it 'should accurately represent Automotive::Vehicle::make' do
    make = @car_example.find('Automotive::Vehicle::make')
    vehicle_maker = make.type
    vehicle_maker.should be_a(UmlMetamodel::Enumeration)
    vehicle_maker.literals.should == []
    vehicle_maker.children.collect{|c| c.name}.should =~ ['DomesticVehicleMaker', 'ForeignVehicleMaker']
    vehicle_maker.all_children.collect{|c| c.name}.should =~ ['DomesticVehicleMaker', 'ForeignVehicleMaker', 'OtherVehicleMaker']
    other_vehicle_maker = @car_example.find('Automotive::OtherVehicleMaker')
    other_vehicle_maker.all_parents.collect{|p| p.name}.should =~ ['DomesticVehicleMaker', 'ForeignVehicleMaker', 'VehicleMaker']
    vehicle_maker.all_literals.collect{|l|l.name}.should =~ ['Ford', 'Dodge', 'Honda', 'Volvo', 'Unknown' ,'Home Made', 'Other']
  end
  
  it 'should be able to list all parents for a classifier in order of proximity' do
    # Test Warrantied interface
    warrantied = @car_example.find('Automotive::Warrantied')
    parent_names = warrantied.all_parents.collect{|p|p.name}
    parent_names.should =~ ['HasWarranty', 'HasSerial']
    
    # Test Vehicle class
    vehicle = @car_example.find('Automotive::Vehicle')
    vehicle.all_parents.should == []
    
    # Test Car class
    car = @car_example.find('Automotive::Car')
    parent_names = car.all_parents.collect{|p|p.name}
    parent_names.should == ['Vehicle']
    
    # Test HybridVehicle class
    hybrid = @car_example.find('Automotive::HybridVehicle')
    parent_names = hybrid.all_parents.collect{|p|p.name}
    parent_names.should =~ ['Car', 'ElectricVehicle', 'Vehicle']
    # Test ordering by proximity
    parent_names[0..1].should =~ ['Car', 'ElectricVehicle']
    parent_names[2].should == 'Vehicle'
  end
  
  it 'should be able to list all parents using the deprecated ancestors method' do
    hybrid = @car_example.find('Automotive::HybridVehicle')
    parent_names = hybrid.all_parents.collect{|p|p.name}
    parent_names.should =~ ['Car', 'ElectricVehicle', 'Vehicle']
  end
  
  it 'should be able to list all implemented interfaces for a classifier in order of proximity' do
    # Test Warrantied interface
    warrantied = @car_example.find('Automotive::Warrantied')
    interface_names = warrantied.all_implements.collect{|p|p.name}
    # Should be empty since interfaces can't implement other interfaces
    interface_names.should == []
    
    # Test Vehicle class
    vehicle = @car_example.find('Automotive::Vehicle')
    interface_names = vehicle.all_implements.collect{|a|a.name}
    interface_names.should =~ ['Warrantied', 'HasSerial', 'HasWarranty']
    # Test ordering by proximity
    interface_names[0].should == 'Warrantied'
    interface_names[1..2].should =~ ['HasSerial', 'HasWarranty']
    
    # Test Car class
    car = @car_example.find('Automotive::Car')
    interface_names = car.all_implements.collect{|a|a.name}
    interface_names.should =~ ['Warrantied', 'HasSerial', 'HasWarranty']
    # Test ordering by proximity
    interface_names[0].should == 'Warrantied'
    interface_names[1..2].should =~ ['HasSerial', 'HasWarranty']
    
    # Test HybridVehicle class
    hybrid = @car_example.find('Automotive::HybridVehicle')
    interface_names = hybrid.all_implements.collect{|p|p.name}
    interface_names.should =~ ['Warrantied', 'HasWarranty', 'HasSerial']
    # Test ordering by proximity
    interface_names[0].should == 'Warrantied'
    interface_names[1..2].should =~ ['HasWarranty', 'HasSerial']
  end
  
  it 'should be able to list all ancestors for a classifier in order of proximity' do
    # Test Warrantied interface
    warrantied = @car_example.find('Automotive::Warrantied')
    ancestor_names = warrantied.all_ancestors.collect{|p|p.name}
    ancestor_names.should =~ ['HasWarranty', 'HasSerial']

    # Test Vehicle class
    vehicle = @car_example.find('Automotive::Vehicle')
    ancestor_names = vehicle.all_ancestors.collect{|a|a.name}
    ancestor_names.should =~ ['Warrantied', 'HasSerial', 'HasWarranty']
    # Test ordering by proximity
    ancestor_names[0].should == 'Warrantied'
    ancestor_names[1..2].should =~ ['HasSerial', 'HasWarranty']
    
    # Test Car class
    car = @car_example.find('Automotive::Car')
    ancestor_names = car.all_ancestors.collect{|a|a.name}
    ancestor_names.should =~ ['Vehicle', 'Warrantied', 'HasSerial', 'HasWarranty']
    # Test ordering by proximity
    ancestor_names[0].should == 'Vehicle'
    ancestor_names[1].should == 'Warrantied'
    ancestor_names[2..3].should =~ ['HasSerial', 'HasWarranty']
    
    # Test HybridVehicle class
    hybrid = @car_example.find('Automotive::HybridVehicle')
    ancestor_names = hybrid.all_ancestors.collect{|a|a.name}
    ancestor_names.should =~ ['Car', 'ElectricVehicle', 'Vehicle', 'Warrantied', 'HasSerial', 'HasWarranty']
    # Test ordering by proximity
    ancestor_names[0..1].should =~ ['Car', 'ElectricVehicle']
    ancestor_names[2].should == 'Vehicle'
    ancestor_names[3].should == 'Warrantied'
    ancestor_names[4..5].should =~ ['HasSerial', 'HasWarranty']
  end
  
  it 'should be able to list all properties for a classifier' do
    # First define what properties should be for each tested classifier
    warrantied_properties = [nil, 'warranty_expiration_date' ,'serial', 'warranty_void', 'replacement_for']
    vehicle_properties = warrantied_properties + ['make', 'vehicle_model', 'cost', 'drivers', 'owners', 'occupants',
      'maintained_by', 'registered_at', 'being_repaired_by', nil, nil]
    car_properties = vehicle_properties + ['miles_per_gallon', 'state', 'inspection_completed']
    electric_vehicle_properties = vehicle_properties + ['electric_efficiency', 'sponsor']
    hybrid_vehicle_properties = car_properties + ['electric_efficiency', 'sponsor'] + ['hybrid_type']
    
    # Test Warrantied interface
    warrantied = @car_example.find('Automotive::Warrantied')
    expect(warrantied.all_properties.collect{|p|p.name}).to contain_exactly(*warrantied_properties)
    # Test Vehicle class
    vehicle = @car_example.find('Automotive::Vehicle')
    expect(vehicle.all_properties.collect{|p|p.name}).to contain_exactly(*vehicle_properties)
    # Test Car class
    car = @car_example.find('Automotive::Car')
    expect(car.all_properties.collect{|p|p.name}).to contain_exactly(*car_properties)
    # Test ElectricVehicle class
    electric_vehicle = @car_example.find('Automotive::ElectricVehicle')
    expect(electric_vehicle.all_properties.collect{|p|p.name}).to contain_exactly(*electric_vehicle_properties)
    # Test HybridVehicle class
    hybrid_vehicle = @car_example.find('Automotive::HybridVehicle')
    expect(hybrid_vehicle.all_properties.collect{|p|p.name}).to contain_exactly(*hybrid_vehicle_properties)
  end
  
  it 'should be able to process primitive types' do
    clown_name = UmlMetamodel.find('People::Clown::Clown::clown_name', @car_example)
    clown_name.type.should == UmlMetamodel::PRIMITIVES[:string]
  end
  
  it 'should accurately represent People::Person::vehicles' do
    vehicles = UmlMetamodel.find('People::Person::vehicles', @car_example)
    vehicles.lower.should == 0
    vehicles.upper.should == Float::INFINITY
    vehicles.multiplicity_string.should == '*'
    vehicles.multiplicity.should == (0..Float::INFINITY)
    vehicles.attribute?.should == false
    vehicles.association?.should == true
    vehicles.singular?.should == false
    vehicles.multiple?.should == true
    vehicles.aggregation.should == :none
    vehicles.composite?.should == false
    vehicles.shared?.should == false
    vehicles.unidirectional?.should == false
    owners = UmlMetamodel.find('Automotive::Vehicle::owners', @car_example)
    owners.lower.should == 1
    owners.upper.should == Float::INFINITY
    vehicles.opposite.should == owners
    vehicles.is_navigable.should == true
    vehicles.is_unique.should == true
    vehicles.is_derived.should == false
    vehicles.is_ordered.should == false
    vehicles.is_globally_unique.should == false
    vehicles.owner.should == UmlMetamodel.find('People::Person', @car_example)
    vehicles.type.should == UmlMetamodel.find('Automotive::Vehicle', @car_example)
    assoc = vehicles.association
    assoc.should be_a(UmlMetamodel::Association)
    assoc.properties.should =~ [vehicles, owners]
    assoc.is_derived.should == false
    assoc_class = assoc.association_class
    assoc_class.should be_a(UmlMetamodel::Class)
    assoc_class.should == UmlMetamodel.find('People::Ownership', @car_example, :type => UmlMetamodel::Class)
    vehicles.default_value.should == nil
  end
  
  it 'should accurately represent Automotive::Vehicle::drivers' do
    drivers = UmlMetamodel.find('Automotive::Vehicle::drivers', @car_example)
    drivers.is_ordered.should == true
  end
  
  it 'should accurately represent People::Ownership::percent_ownership' do
    percent_ownership = UmlMetamodel.find('People::Ownership::percent_ownership', @car_example)
    percent_ownership.default_value.should == 100.0
  end
  
  it 'should accurately represent Automotive::Part::sub_parts' do
    property = UmlMetamodel.find('Automotive::Part::sub_parts', @car_example)
    property.aggregation.should == :composite
    property.composite?.should == true
    property.shared?.should == false
    property.lower.should == 0
    property.upper.should == Float::INFINITY
    property.visibility.should == :public
    property.is_navigable.should be_truthy
    property.is_derived.should be_falsey
    property.is_globally_unique.should be_falsey
    property.id.class.should == String
  end
  
  it 'should accurately represent Automotive::HybridVehicle' do
    hv = UmlMetamodel.find('Automotive::HybridVehicle', @car_example)
    ev = UmlMetamodel.find('Automotive::ElectricVehicle', @car_example)
    c = UmlMetamodel.find('Automotive::Car', @car_example)
    v = UmlMetamodel.find('Automotive::Vehicle', @car_example)
    hv.parents.should =~ [ev, c]
    c.parents.should == [v]
    ev.parents.should == [v]
    c.children.should include(hv)
    ev.children.should include(hv)
  end
  
  it 'should accurately represent Automotive::Vehicle (root behavior test)' do
    vehicle = UmlMetamodel.find('Automotive::Vehicle', @car_example)
    # Test root_extensions (root?/root_classifiers)
    vehicle.root?.should == true
    automotive = UmlMetamodel.find('Automotive', @car_example)
    automotive.root_classifiers.should include(vehicle)
    @car_example.root_classifiers.should == []
    @car_example.root_classifiers(:recurse => true, :include_imports => true).should include(vehicle)
    # Test root_package
    vehicle.root_package.should == automotive
  end
  
  it 'should be able to replace references to Gui_Builder_Profile or XSD Primitives packages with UML equivalents' do
    require 'uml_metamodel/fixes'
    # Check before fixing
    gbp_date = @car_example.find('Gui_Builder_Profile::Date')
    richtext = @car_example.find('Gui_Builder_Profile::RichText')
    @car_example.find('People::Person::date_of_birth').type.should == gbp_date
    @car_example.find('People::Driving::car_review').type.should == richtext
    
    # Fix elements
    UmlMetamodel.fix_project_elements!(@car_example)
    
    # Test that GBP has been removed
    @car_example.find('Gui_Builder_Profile::Date').should be_nil
    @car_example.find('Gui_Builder_Profile').should be_nil
    
    # Test that types have been fixed
    @car_example.find('People::Person::date_of_birth').type.should == UmlMetamodel::PRIMITIVES[:date]
    @car_example.find('People::Driving::car_review').type.should == UmlMetamodel::PRIMITIVES[:string]
  end
end
# rubocop:enable Metrics/BlockLength, Lint/Void
