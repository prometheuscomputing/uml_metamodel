require 'spec_helper'
describe 'CarExample Metamodel diffing functionality' do
  before(:all) do
    @car_example_dsl_file = File.join(TEST_DATA, 'CarExampleApplication.rb')
    # Deserialize from Marshal
    @car_example = UmlMetamodel.from_dsl_file(@car_example_dsl_file, :suppress_warnings => false)
  end
  
  it 'should correctly match objects that are functionaly identical' do
    expect(@car_example.matches?(@car_example)).to be_truthy
    same_example = UmlMetamodel.from_dsl_file(@car_example_dsl_file, :suppress_warnings => false)
    expect(@car_example.matches?(same_example)).to be_truthy
  end
  
  it 'should match object trees that are functionaly identical' do
    expect(@car_example.matches?(@car_example, true)).to be_truthy
    same_example = UmlMetamodel.from_dsl_file(@car_example_dsl_file, :suppress_warnings => false)
    expect(@car_example.matches?(same_example, true)).to be_truthy
  end

  # TODO create a more comprehensive set of examples.
  it 'should not match object trees that are not functionaly identical' do
    different_example = UmlMetamodel.from_dsl_file(@car_example_dsl_file, :suppress_warnings => false)
    different_example.find("People::Person").name = "Dude"
    expect(@car_example.matches?(different_example, true)).to be_falsey
    
    different_example = UmlMetamodel.from_dsl_file(@car_example_dsl_file, :suppress_warnings => false)
    different_example.find("People::Person::name").name = "monicker"
    expect(@car_example.matches?(different_example, true)).to be_falsey

    different_example = UmlMetamodel.from_dsl_file(@car_example_dsl_file, :suppress_warnings => false)
    different_example.find("People::Person").associations.first.properties.reverse!
    expect(@car_example.matches?(different_example, true)).to be_falsey

    different_example = UmlMetamodel.from_dsl_file(@car_example_dsl_file, :suppress_warnings => false)
    different_example.find("People::Person").id.reverse!
    expect(@car_example.matches?(different_example, true)).to be_falsey

    different_example = UmlMetamodel.from_dsl_file(@car_example_dsl_file, :suppress_warnings => false)
    different_example.find("Application").applied_stereotypes.find { |ast| ast.instance_of.name == "options"}.applied_tags.first.value = "different value"
    expect(@car_example.matches?(different_example, true)).to be_falsey
  end

  # TODO create a more comprehensive set of examples.
  it 'should match objects that are functionaly identical but which differences within their object trees' do
    different_example = UmlMetamodel.from_dsl_file(@car_example_dsl_file, :suppress_warnings => false)
    different_example.find("People::Person").name = "Dude"
    expect(@car_example.matches?(different_example)).to be_truthy
    
    different_example = UmlMetamodel.from_dsl_file(@car_example_dsl_file, :suppress_warnings => false)
    different_example.find("People::Person::name").name = "monicker"
    expect(@car_example.matches?(different_example)).to be_truthy
    
    
    different_example = UmlMetamodel.from_dsl_file(@car_example_dsl_file, :suppress_warnings => false)
    different_example.find("People::Person").associations.first.properties.reverse!
    expect(@car_example.matches?(different_example)).to be_truthy
    
    different_example = UmlMetamodel.from_dsl_file(@car_example_dsl_file, :suppress_warnings => false)
    different_example.find("People::Person").id.reverse!
    expect(@car_example.matches?(different_example)).to be_truthy

    different_example = UmlMetamodel.from_dsl_file(@car_example_dsl_file, :suppress_warnings => false)
    different_example.find("Application").applied_stereotypes.find { |ast| ast.instance_of.name == "options"}.applied_tags.first.value = "different value"
    expect(@car_example.matches?(different_example)).to be_truthy
  end

  
end