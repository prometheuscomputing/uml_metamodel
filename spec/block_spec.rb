require 'spec_helper'

describe 'DSL ability to parse blocks not found in CarExample' do

  it 'should be able to read a DSL project and write if back out again' do
    # Deserialize from DSL
    file = File.join(TEST_DATA, 'BlocksTest.rb')
    uml  = UmlMetamodel.from_dsl_file(file, :suppress_warnings => false)
    
    new_file = File.join(TMP_DIR, 'BockTestsOutput.rb')
    FileUtils.rm(new_file) if File.exists?(new_file)
    uml.to_dsl_file(new_file)
    File.read(file).should == File.read(new_file)
  end

end
