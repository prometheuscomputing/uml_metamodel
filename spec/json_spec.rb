require 'spec_helper'

describe 'JSON functionality' do
  
  it 'The #to_json methods should work' do
    # Deserialize from DSL
    car_example_dsl_file = File.join(TEST_DATA, 'CarExampleApplication.rb')
    car_example = UmlMetamodel.from_dsl_file(car_example_dsl_file, :suppress_warnings => false)
    
    # Reserialize to JSON
    File.open(File.join(TMP_DIR, 'car_example.json'), "w+") { |f| f << JSON.pretty_generate(car_example.to_json_obj) }
  end
  
  # This test is dependent on the previous test for the #to_json methods having passed
  it 'The JSON parsing methods should work' do
    project = UmlMetamodel::JSON.parse_project_file(File.join(TMP_DIR, 'car_example.json'))
    File.open(File.join(TMP_DIR, 'car_example_from_json.rb'), "w+") { |f| f << project.to_dsl }
    start  = File.read(File.join(TEST_DATA, 'CarExampleApplication.rb'))
    result = File.read(File.join(TMP_DIR, 'car_example_from_json.rb'))
    # fails w/o #strip even though diff tools will show no difference
    expect(start.strip).to eq result.strip
  end
end
