require 'spec_helper'

describe 'metamodel functionality' do
  before(:all) do
    @dsl_file = File.join(TEST_DATA, 'AssocTest.rb')
    # Deserialize from Marshal
    @example = UmlMetamodel.from_dsl_file(@dsl_file, :suppress_warnings => false)
  end

  context 'Property and Association serialization and deserialization' do
    
    it 'should be able to add properties to associations based on property ids' do
      pkg1 = UmlMetamodel.find('Pkg1', @example)
      pkg1.associations.first.properties.size.should == 2
    end
    
    it 'should properly serialize elements that involve a property with no name' do
      pkg1 = UmlMetamodel.find('Pkg1', @example)
      klass = pkg1.classifiers.first
      klass.properties.first.to_dsl.should == 'property nil, :type => "Pkg2::Class2", :id => "property1"'
      pkg1.associations.first.to_dsl.should == 'association :properties => ["property1", "property2"], :id => "assoc1"'
    end
        
  end
end
