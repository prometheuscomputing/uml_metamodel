project "BlockTest" do
  package "BlockFun" do
    association :properties => ["BlockFun::FunClass::fun", "BlockFun::MoreFunClass::more_fun"] do
      applied_stereotype :instance_of => "BlockFun::Fun" do
        applied_tag :instance_of => "BlockFun::Fun::fun_tag", :value => "The Association's Fun Tag"
      end
    end
    klass "FunClass" do
      property "fun", :type => "BlockFun::MoreFunClass", :id => "funid1" do
        applied_stereotype :instance_of => "BlockFun::Fun" do
          applied_tag :instance_of => "BlockFun::Fun::fun_tag", :value => "FunClass::fun's Fun Tag"
        end
      end
    end
    klass "MoreFunClass" do
      property "more_fun", :type => "BlockFun::FunClass", :id => "funid2"
    end
    enumeration "MyEnum" do
      property "value", :type => "UML::String"
      literal "Lit1" do
        applied_stereotype :instance_of => "BlockFun::Fun" do
          applied_tag :instance_of => "BlockFun::Fun::fun_tag", :value => "Lit1's Fun Tag"
        end
      end
      literal "Lit2" do
        applied_stereotype :instance_of => "BlockFun::Fun" do
          applied_tag :instance_of => "BlockFun::Fun::fun_tag", :value => "Lit2's Fun Tag"
        end
      end
    end
    stereotype "Fun" do
      tag "fun_tag" do
        applied_stereotype :instance_of => "BlockFun::Fun" do
          applied_tag :instance_of => "BlockFun::Fun::fun_tag", :value => "Really? This worked?  Holy Moly!"
        end
      end
    end
    stereotype "MoreFun" do
      applied_stereotype :instance_of => "BlockFun::Fun" do
        applied_tag :instance_of => "BlockFun::Fun::fun_tag", :value => "MoreFun's Fun Tag"
      end
      tag "more_fun_tag" do
        applied_stereotype :instance_of => "BlockFun::Fun" do
          applied_tag :instance_of => "BlockFun::Fun::fun_tag", :value => "more_fun_tag's Fun Tag"
        end
      end
    end
  end
end
