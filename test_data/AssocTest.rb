project "AssocTest" do
  package "Pkg1", :id => "pkg1" do
    association :properties => ["property1", "property2"], :id => "assoc1"
    klass "Class1", :id => "class1" do
      property nil, :type => "Pkg2::Class2", :id => "property1"
    end
  end
  package "Pkg2", :id => "pkg2" do
    klass "Class2", :id => "class2" do
      property nil, :type => "Pkg1::Class1", :id => "property2"
    end
  end
end
