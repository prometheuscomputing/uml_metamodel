project "AssocClassTest", :additional_info => {:selected_model=>{:name=>"Application", :id=>"_17_0_2_4_b9202e7_1417447546518_583867_2182"}}, :id => "_18_0_2_b9202e7_1522429186606_911412_4627" do
  model "Application", :imports => ["Automotive", "People"], :id => "_17_0_2_4_b9202e7_1417447546518_583867_2182" do
  end
  package "Automotive", :id => "_16_9_78e0236_1366066056048_138330_1616" do
    klass "Vehicle", :implements => ["Automotive::Warrantied"], :id => "_16_9_78e0236_1366067167425_72606_2065", :is_abstract => true do
      property "drivers", :type => "People::Person", :is_ordered => true, :upper => Float::INFINITY, :id => "_16_9_78e0236_1366067180171_210074_2092"
    end
  end
  package "People", :id => "_16_9_78e0236_1366066083021_757821_1617" do
    association :properties => ["People::Person::drives", "Automotive::Vehicle::drivers"], :association_class => "People::Driving", :id => "ac__16_9_78e0236_1366067180171_151003_2089"
    klass "Driving", :id => "_16_9_78e0236_1366067180171_151003_2089"
    klass "Person", :id => "_16_9_78e0236_1366066203528_181267_1642" do
      property "drives", :type => "Automotive::Vehicle", :is_ordered => true, :upper => Float::INFINITY, :id => "_16_9_78e0236_1366067180171_831195_2091"
    end
  end
end
