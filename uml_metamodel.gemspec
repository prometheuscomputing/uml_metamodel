# frozen_string_literal: true

# This file was generated programmatically on 18-APR-2022 using the `package_gem` functionality of mmlite.

Gem::Specification.new do |s|
  s.name        = "uml_metamodel"
  s.version     = "3.5.0"
  s.authors     = ["Michael Faughn", "Sam Dana"]
  s.email       = ["michael.faughn@nist.gov", "s.dana@prometheuscomputing.com"]
  s.homepage    = "https://gitlab.com/prometheuscomputing/uml_metamodel"
  s.summary     = "A ruby implementation of the UML Metamodel."
  s.description = "A ruby implementation of the UML Metamodel. While this project is not intended to be strictly adherent to the OMG UML Metamodel specification, it does provide the most of the UML Metamodel components used by UML Class Diagrams."
  s.licenses    = ['MPL-2.0']
  s.files = [
      "LICENSE",
      "README.rdoc",
      "lib/uml_metamodel.rb",
      "lib/uml_metamodel/json.rb",
      "lib/uml_metamodel/root_extensions.rb",
      "lib/uml_metamodel/validation.rb",
      "lib/uml_metamodel/meta_info.rb",
      "lib/uml_metamodel/find.rb",
      "lib/uml_metamodel/diff.rb",
      "lib/uml_metamodel/uml_package.rb",
      "lib/uml_metamodel/elements/project.rb",
      "lib/uml_metamodel/elements/tag.rb",
      "lib/uml_metamodel/elements/datatype.rb",
      "lib/uml_metamodel/elements/has_searchable_contents.rb",
      "lib/uml_metamodel/elements/enumeration_literal.rb",
      "lib/uml_metamodel/elements/class.rb",
      "lib/uml_metamodel/elements/association.rb",
      "lib/uml_metamodel/elements/model.rb",
      "lib/uml_metamodel/elements/applied_tag.rb",
      "lib/uml_metamodel/elements/primitive.rb",
      "lib/uml_metamodel/elements/serializable.rb",
      "lib/uml_metamodel/elements/container_element.rb",
      "lib/uml_metamodel/elements/element.rb",
      "lib/uml_metamodel/elements/packageable.rb",
      "lib/uml_metamodel/elements/stereotype.rb",
      "lib/uml_metamodel/elements/interface.rb",
      "lib/uml_metamodel/elements/profile.rb",
      "lib/uml_metamodel/elements/classifier.rb",
      "lib/uml_metamodel/elements/applied_stereotype.rb",
      "lib/uml_metamodel/elements/property.rb",
      "lib/uml_metamodel/elements/enumeration.rb",
      "lib/uml_metamodel/elements/element_base.rb",
      "lib/uml_metamodel/elements/package.rb",
      "lib/uml_metamodel/reserved_words.rb",
      "lib/uml_metamodel/elements.rb",
      "lib/uml_metamodel/fixes.rb",
      "lib/uml_metamodel/attr_assoc.rb",
      "lib/uml_metamodel/dsl.rb"
  ]
  s.test_files = [
      "spec/spec_helper.rb",
      "spec/matches_spec.rb",
      "spec/json_spec.rb",
      "spec/normal_irb",
      "spec/assoc_by_id_spec.rb",
      "spec/block_spec.rb",
      "spec/uml_metamodel_spec.rb",
      "test_data/AssocClassTest.rb",
      "test_data/AssocTest.rb",
      "test_data/CarExampleApplication.rb",
      "test_data/BlocksTest.rb",
  ]
  s.add_dependency("indentation")
  s.add_dependency("cleanroom", "~> 1.0")
  s.add_dependency("rainbow", "~> 3.0")
  s.add_development_dependency("rspec")
  s.add_development_dependency("simplecov")
end
