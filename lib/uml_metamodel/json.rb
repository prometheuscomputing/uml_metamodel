require 'json'
require_relative 'elements'
module UmlMetamodel  
  class JSON
    attr_accessor :top_level_elements
    
    TYPES = {
      :applied_stereotypes => UmlMetamodel::AppliedStereotype,
      :applied_tags        => UmlMetamodel::AppliedTag,
      :associations        => UmlMetamodel::Association,
      :classes             => UmlMetamodel::Class,
      :datatypes           => UmlMetamodel::Datatype,
      :literals            => UmlMetamodel::EnumerationLiteral,
      :enumerations        => UmlMetamodel::Enumeration,
      :interfaces          => UmlMetamodel::Interface,
      :models              => UmlMetamodel::Model,
      :packages            => UmlMetamodel::Package,
      :primitives          => UmlMetamodel::Primitive,
      :profiles            => UmlMetamodel::Profile,
      :projects            => UmlMetamodel::Project,
      :properties          => UmlMetamodel::Property,
      :stereotypes         => UmlMetamodel::Stereotype,
      :tags                => UmlMetamodel::Tag
    }
    
    def initialize(json, options)
      @json     = json
      @options  = options
    end
    
    def self.parse_project_file(file_path, options = {})
      json = ::JSON.parse(File.read(file_path), :symbolize_names => true)
      parse_project(json, options)
    end
    
    def self.parse_project(json, options = {})
      new(json, options).parse_project
    end
    
    def parse_project(options = {})
      @project = UmlMetamodel::Project.new_from_json(@json)
      create_umm_elements(UmlMetamodel::Project, @json, options)
      args = [@project, @json, options]
      containerize_umm_elements(*args)
      add_imports(*args)
      apply_stereotypes(*args)
      type_applied_stereotypes(*args)
      finish_properties(*args)
      add_generalizers_and_interfaces(*args)
      add_properties_to_associations(*args)
      connect_association_classes(*args)
      @project
    end
    
    def connect_association_classes(element, json, options = {})
      element.default_contents_order.each do |key|
        j_objs = json[stash_key(key)]
        next unless j_objs
        j_objs.each do |j_obj|
          obj = j_obj[:obj]
          if obj.is_a?(UmlMetamodel::Association) && j_obj[:_association_class]
            obj.association_class = UmlMetamodel.find(j_obj[:_association_class], @project)
          else
            connect_association_classes(obj, j_obj)
          end
        end
      end
    end
    
    def add_generalizers_and_interfaces(element, json, options = {})
      element.default_contents_order.each do |key|
        j_objs = json[stash_key(key)]
        next unless j_objs
        j_objs.each do |j_obj|
          obj = j_obj[:obj]
          if j_obj[:_parents]
            obj.parents = j_obj[:_parents].collect { |parent| UmlMetamodel.find(parent, @project) }
          end
          if j_obj[:_implements]
            obj.implements = j_obj[:_implements].collect { |iface| UmlMetamodel.find(iface, @project) }
          end
          add_generalizers_and_interfaces(obj, j_obj)
        end
      end
    end
    
    def add_imports(element, json, options = {})
      element.default_contents_order.each do |key|
        j_objs = json[stash_key(key)]
        next unless j_objs
        j_objs.each do |j_obj|
          obj = j_obj[:obj]
          if j_obj[:_imports]
            obj.imports = j_obj[:_imports].collect { |import| UmlMetamodel.find(import, @project) }
          end
          add_imports(obj, j_obj)
        end
      end
    end
    
    def apply_stereotypes(element, json, options = {})
      element.default_contents_order.each do |key|
        j_objs = json[stash_key(key)]
        next unless j_objs
        j_objs.each do |j_obj|
          obj = j_obj[:obj]
          if j_obj[:_applied_stereotypes]
            applied_stereotype_objs = j_obj[:_applied_stereotypes].collect do |st|
              as_uml_element = st[:obj]

              stereotype = UmlMetamodel.find(st[:_instance_of], @project)
              raise "Could not find a Stereotype matching #{st[:_instance_of]}" unless stereotype
              as_uml_element.instance_of = stereotype

              applied_tags = st[:_applied_tags]
              at_uml_elements = [st[:_applied_tags]].compact.flatten.collect do |at|
                at_uml_element = at[:obj]
                tag = UmlMetamodel.find(at[:_instance_of], @project)
                raise "Could not find a Tag matching #{st[:_instance_of]}" unless tag
                at_uml_element.instance_of = tag
                at_uml_element
              end
              as_uml_element.applied_tags = at_uml_elements if at_uml_elements.any?

              as_uml_element
            end
            obj.applied_stereotypes = applied_stereotype_objs
          end
          apply_stereotypes(obj, j_obj)
        end
      end
    end
    
    def type_applied_stereotypes(element, json, options = {})
      element.default_contents_order.each do |key|
        j_objs = json[stash_key(key)]
        next unless j_objs
        j_objs.each do |j_obj|
          obj = j_obj[:obj]
          if obj.is_a?(UmlMetamodel::AppliedStereotype) || obj.is_a?(UmlMetamodel::AppliedTag)
            obj.instance_of = UmlMetamodel.find(j_obj[:_instance_of], @project)
          end
          type_applied_stereotypes(obj, j_obj)
        end
      end
    end
    
    def finish_properties(element, json, options = {})
      element.default_contents_order.each do |key|
        j_objs = json[stash_key(key)]
        next unless j_objs
        j_objs.each do |j_obj|
          obj = j_obj[:obj]
          if obj.is_a?(UmlMetamodel::Property)
            obj.type = UmlMetamodel.find(j_obj[:_type], @project)
            fix_property_properties(obj)            
          else
            finish_properties(obj, j_obj)
          end
        end
      end
    end
    
    def fix_property_properties(obj)
      obj.upper = Float::INFINITY if obj.upper == 'infinity'
      if obj.default_value
        if obj.type == UmlMetamodel::PRIMITIVES[:boolean]
          if obj.default_value == 'true'
            obj.default_value = true
          end
          if obj.default_value == 'false'
            obj.default_value = false
          end
        end
        UmlMetamodel::PRIMITIVES[:integer]
        if obj.type == UmlMetamodel::PRIMITIVES[:integer] || obj.type == UmlMetamodel::PRIMITIVES[:unlimited_natural]
          obj.default_value = obj.default_value.to_i
        end
        if obj.type == UmlMetamodel::PRIMITIVES[:float]
          obj.default_value = obj.default_value.to_f
        end
      end
    end
    private :fix_property_properties
    
    def add_properties_to_associations(element, json, options = {})
      element.default_contents_order.each do |key|
        j_objs = json[stash_key(key)]
        next unless j_objs
        j_objs.each do |j_obj|
          obj = j_obj[:obj]
          if obj.is_a?(UmlMetamodel::Association)
            obj.properties = j_obj[:_properties].collect { |prop| UmlMetamodel.find(prop, @project) }
          else
            add_properties_to_associations(obj, j_obj)
          end
        end
      end
    end
    
    # Recursive
    def create_umm_elements(type, json, options = {})
      # keys represent UML element types
      type.default_contents_order.each do |key|
        j_objs = json.delete(key)
        json[stash_key(key)] = j_objs
        next unless j_objs
        # Everything we are parsing is contained in something (we create the Project separately and it is the uber-container).  Each json key maps to a UmlMetamodel element class
        contained_type = get_type_from_json_key(key)
        j_objs.each do |j_obj|
          # Here is the recursion
          obj = create_umm_elements(contained_type, j_obj, options)
        end
        # Put what was just created back into the json using a key that is the same as the original key but prepended with '_'.  e.g. 'properties' becomes '_properties'.
        # It is done like this because many of the UmlMetamodel element constructors parse an options hash and will act on the same keys that the original json hash used.  We want to make sure that we don't get unexpected behavior in those constructors so we are making sure that the keys we are keeping things in are not going to be parsed in those constructors.
        json[stash_key(key)] = j_objs
      end
      original_json = json.dup
      stash_additional_options(json, options)
      # Here is where we are passin the json to the UmlMetamodel constructor and why we needed to use different names for the keys
      json[:obj] = type.new_from_json(json)
    end
    
    # helps w/ debugging...
    def inspect(element, json)
      element.default_contents_order.each do |key|
        j_objs = json[stash_key(key)]
        next unless j_objs
        j_objs.each do |j_obj|
          obj = j_obj[:obj]
          puts "#{obj.class} -- #{obj.name}"
          inspect(obj, j_obj)
        end
      end
    end
    
    def containerize_umm_elements(container, json, options = {})
      container.default_contents_order.each do |key|
        j_objs = json[stash_key(key)]
        next unless j_objs
        j_objs.each do |j_obj|
          obj = j_obj[:obj]
          containerize_umm_elements(obj, j_obj, options)
          # FIXME maybe?  This is not very nice.  We created the 'value' property for enumerations but they already get created automatically so we are going to just ignore the ones we created earlier during parsing.  This leave them hanging around for a while....
          container.add_element(obj) unless container.is_a?(UmlMetamodel::Enumeration) && obj.name == 'value'
        end
      end
    end
    
    def stash_additional_options(json, options)
      keys = [:parents, :type, :instance_of, :imports, :properties, :implements, :association_class]
      keys.each { |key| val = json.delete(key); json[stash_key(key)] = val if val }
    end
    
    def stash_key(key)
      ('_' + key.to_s).to_sym
    end
    private :stash_key
    
    def get_type_from_json_key(key)
      TYPES[key.to_sym]
    end
    private :get_type_from_json_key
      
  end
  
  class ElementBase
    protected
    def sorted_json(element_type, options)
      custom_option   = ("custom_json_#{element_type}").to_sym
      unsorted_option = ("#{element_type}_unsorted").to_sym
      if options[custom_option]
        send(options[custom_option])
      elsif options[:unsorted] || options[unsorted_option]
        send(element_type)
      else
        if element_type == "applied_stereotypes"
          send(element_type).sort_by { |e| e.qualified_name.to_s }
        else
          specific_type = UmlMetamodel::JSON::TYPES[element_type]
          send(element_type).select { |e| e.class == specific_type }.sort_by { |e| e.name.to_s }
        end
      end
    end
    
    def add_json_content(json, options)
      json_contents_order = options[('custom_' + serialization_name + '_order').to_sym] || default_contents_order
      json_contents_order.each do |element_type|
        elements = sorted_json(element_type, options)
        next unless elements.any?
        json[element_type.to_s] = elements.collect { |e| e.to_json_obj }
      end
    end
  end
end
