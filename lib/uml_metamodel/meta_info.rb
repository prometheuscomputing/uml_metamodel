module UmlMetamodel
  GEM_NAME = "uml_metamodel"
  VERSION  = '3.5.0'
  AUTHORS  = ["Michael Faughn", "Sam Dana"]
  SUMMARY = %q{A ruby implementation of the UML Metamodel.}
  EMAILS      = ["michael.faughn@nist.gov", "s.dana@prometheuscomputing.com"]
  HOMEPAGE    = 'https://gitlab.com/prometheuscomputing/uml_metamodel'
  DESCRIPTION = %q{A ruby implementation of the UML Metamodel. While this project is not intended to be strictly adherent to the OMG UML Metamodel specification, it does provide the most of the UML Metamodel components used by UML Class Diagrams.}
  
  LANGUAGE         = :ruby
  LANGUAGE_VERSION = ['>= 2.7']
  RUNTIME_VERSIONS = { :mri => ['>= 2.7'] }
  TYPE     = :library
  LAUNCHER = nil
    
  DEPENDENCIES_RUBY = {
    :indentation => nil,
    :cleanroom   => '~> 1.0',
    :rainbow     => '~> 3.0'
  }
  DEVELOPMENT_DEPENDENCIES_RUBY  = {
    :rspec => nil,
    :simplecov => nil
  }
end
