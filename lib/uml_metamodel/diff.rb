# FIXME unfinished and untested
module UmlMetamodel
  def self.diff_from_files(file1, file2, options = {})
    elements1 = from_dsl_file(file1)
    elements2 = from_dsl_file(file2)
    Diff.diff_elements(elements1, elements2)
  end
  
  def self.diff_from_dsl(dsl1, dsl2, options = {})
    file_path1 = options.delete[:file_path1]
    file_path2 = options.delete[:file_path2]
    elements1 = DSL.parse(dsl1, file_path1, options)
    elements2 = DSL.parse(dsl2, file_path2, options)
    Diff.diff_elements(elements1, elements2, options)
  end
  
  def self.diff_elements(elements1, elements1, options = {})
    Diff.diff_elements(elements1, elements2, options)
  end
  
  class Diff
    def self.diff_elements(elements1, elements1, options = {})
      new(elements1, elements1, options = {})
    end
        
    def initialize(elements1, elements2, options = {})
      @elements1    = elements1
      @elements2    = elements2
      @registry1    = register_elements(elements1, options)
      @registry2    = register_elements(elements2, options)
      @common_ids   = @registry1[:by_id].keys & @registry2[:by_id].keys
      @common_names = @registry1[:by_name].keys & @registry2[:by_name].keys
      @common_ids_and_names = @common_ids.collect{ |id| [id, by_id1(id).qualified_name] if by_id1(id).qualified_name == by_id2(id).qualified_name }.compact
      diff(options)
    end  
    
    private
    
    def diff(options = {})
      filename = options[:output_path]
      report = ''
      report << diff_by_name_report
      report << diff_by_id_report
      report << associations_report
      report << inheritance_report
      report << properties_report
      report << stereotypes_report
      puts report
      File.open(filename, w+) { |f| f.puts report } if filename
    end
        
    def by_name1(val); @registry1[:by_name][val]; end
    def by_name2(val); @registry1[:by_name][val]; end
    def by_id1(val);   @registry1[:by_id][val];   end
    def by_id2(val);   @registry1[:by_id][val];   end  
    
    def 
    
    def diff_associations
      # TODO what makes an association different?
      assocs1 = @registry1[:by_type][UmlMetamodel::Association]
      assocs2 = @registry2[:by_type][UmlMetamodel::Association]
      @common_assocs_by_id = []
      assocs1.each { |a1| @common_assocs_by_id << a1.id if assocs2.find { |a2| a2.id == a1.id } }
      @uniq_assocs1 = assocs1.select { |a| !(@common_assocs_by_id.include?(a.id))}
      @uniq_assocs2 = assocs2.select { |a| !(@common_assocs_by_id.include?(a.id))}
      
    end
    
    def associations_report
      
    end
    
    def diff_inheritance
      
    end
    
    def inheritance_report
      
    end    
    
    def diff_properties
      @properties1 = @registry1[:by_type][UmlMetamodel::Property]
      @properties2 = @registry2[:by_type][UmlMetamodel::Property]
      @common_properties_by_id   = []
      @common_properties_by_name = []
      @properties1.each do |p1|
        p2_by_id = @properties2.find { |p2| p2.id == p1.id }
        @common_properties_by_id << [p1,p2] if p2_by_id
        p2_by_name = @properties2.find { |p2| p2.qualified_name == p1.qualified_name }
        @common_properties_by_name << [p1,p2] if p2_by_name
      end
    end
    
    def properties_report
      
      
    end
    
    def diff_stereotypes
      
    end
    
    def stereotypes_report
      
    end
        
    def diff_qualified_names
      @names_unique_to_1 = [@registry1[:by_name].keys - @registry2[:by_name].keys].sort
      @names_unique_to_2 = [@registry2[:by_name].keys - @registry1[:by_name].keys].sort
      @changed_names = []
      common_ids.each do |id|
        names = [@registry1[:by_id][id].qualified_name, @registry2[:by_id][id].qualified_name]
        @changed_names << names if names.first != names.last
      end
    end
    
    def diff_by_name_report
      diff_qualified_names
      report = []
      if @names_unique_to_1.any?
        report << 'Qualified Names unique to A:'
        @names_unique_to_1.each { |n| report << "  #{by_name1(n).class}: #{n}" }
        report << "\n"
      end
      if @names_unique_to_2.any?
        report << 'Qualified Names unique to B:'
        @names_unique_to_2.each { |n| report <<"  #{by_name2(n).class}: #{n}" }
        report << "\n"
      end
      if @changed_names.any?
        report << 'Qualified Names known to have changed from A to B:'
        @changed_names.each do |name_pair|
          n1     = name_pair.first
          n2     = name_pair.last
          etype1 = by_name1(n1).class
          etype2 = by_name2(n2).class
          etype  = etype1 == etype2 ? etype1.name.demodulize : "ELEMENT TYPE CHANGED"
          report << "  #{etype}: #{n1} -> #{n2}"
        end
        report << "\n"
      end
      report.join("\n")
    end

    def diff_ids
      @ids_unique_to_1 = [@registry1[:by_id].keys - @registry2[:by_id].keys].sort
      @ids_unique_to_2 = [@registry2[:by_id].keys - @registry1[:by_id].keys].sort
      @changed_ids = []
      common_names.each do |name|
        ids = [name, @registry1[:by_name][name].id, @registry2[:by_name][name].id]
        @changed_ids << ids if ids[1] != ids.last
      end
    end
    
    def diff_by_id_report
      diff_ids
      report = []
      if @ids_unique_to_1.any?
        report << 'IDs unique to A:'
        @ids_unique_to_1.each { |id| report << "  #{by_id1(id).class.demodulize}: #{by_id1(id).qualified_name} -- #{i}" }
        report << "\n"
      end
      if @ids_unique_to_2.any?
        report << 'IDs unique to B:'
        @ids_unique_to_2.each { |id| report << "  #{by_id2(id).class.demodulize}: #{by_id2(id).qualified_name} -- #{i}" }
        report << "\n"
      end
      if @changed_ids.any?
        report << 'IDs known to have changed from A to B:'
        @changed_ids.each do |ids|
          name = ids[0]
          id1  = ids[1]
          id2  = ids[2]
          etype1 = by_id1(id1).class
          etype2 = by_id2(id2).class
          etype = etype1 == etype2 ? etype1.name.demodulize : "ELEMENT TYPE CHANGED"
          report << "  #{etype}: #{name}  #{id1} -> #{id2}"
        end
        report << "\n"
      end
      report.join("\n")
    end

    def register_elements(elements, options = {})
      registry = { :by_name => {}, :by_id => {}, :by_type => {} }
      Array(elements).each { |element| register_element(registry, element, options) }
      registry
    end
    
    def register_element(registry, element, options = {})
      registry[:by_name][element.qualified_name] 
      registry[:by_id][element.id] = element if element.id
      registry[:by_type][element.class] ||= []
      registry[:by_type][element.class] << element
      case element
      when UmlMetamodel::Project, UmlMetamodel::Package
        element.contents.each { |e| register_element(registry, e, options) }
      when UmlMetamodel::Enumeration
        element.literals.each { |lit| register_element(registry, lit, options) }
        # should we be ignoring the value property?
        element.properties.each { |prop| register_element(registry, prop, options) } # unless prop.name == 'value'}
      when UmlMetamodel::Classifier
        element.properties.each { |prop| register_element(registry, prop, options) }
      when UmlMetamodel::Stereotype
        element.tags.each { |tag| register_element(registry, tag, options) }
      end
    end
    
  end
end