# TODO: move this to another project if this works out -SD
class Module
  # Defines an association from one Ruby object to other Ruby objects that is maintained bi-directionally
  # Options are:
  #   :inverse - The inverse name for the association
  #   :singular - The singularized name of the association (defaults to name)
  #   :inverse_singular - The singularized inverse name for the association (defaults to inverse name)
  #   :multiple - Whether this association is multiple
  #   :inverse_multiple - Whether the inverse association is multiple
  #   :types - If specified, all objects added to this association are required to be one of these type classes
  # The following public methods are defined for a given association end:
  #   <name> - Returns the association value
  #   _<name> - Returns an array of the associated objects regardless of multiplicity
  #   <name>= - Sets the association value
  #   _<name>= - Sets the associated objects with an array regardless of multiplicity
  #   add_<singular> - Adds a bi-directional association to an object, running any hooks and breaking existing associations if needed
  #   remove_<singular> - Removes a bi-directional association to an object
  # Associations should currently only contain unique elements
  def attr_assoc(name, options = {})
    name = name
    inverse_name = options[:inverse]
    raise "You must specify an inverse association name" unless inverse_name
    singular = options[:singular] || name
    inverse_singular = options[:inverse_singular] || options[:inverse]
    multiple = options[:multiple] || false
    inverse_multiple = options[:inverse_multiple] || false
    # An array of allowed types for this association
    # TODO: this needs to convert strings to constants
    types = options[:types]
    
    # Returns the associated object
    # This is either a single value or an array of values depending on whether this association is multiple
    define_method(name) {
      value = send("_#{name}")
      multiple ? value : value.first
    }
    # Returns an array of associated objects
    define_method("_#{name}") {
      instance_variable_set("@#{name}".to_sym, []) if instance_variable_get("@#{name}".to_sym).nil?
      instance_variable_get("@#{name}".to_sym)
    }
    
    # Set the association value
    # The argument should be either a single value or an array of values depending on whether this association is multiple
    define_method("#{name}=") {|elements|
      send("_#{name}=", Array(elements))
    }
    # Set the associated objects
    define_method("_#{name}=") {|elements|
      # Remove all existing associations
      send("_#{name}").each {|e| send("remove_#{singular}", e)}
      # Add associations to elements in arg
      elements.each {|e| send("add_#{singular}", e)}
    }
    
    # Define bi-directional association links by invoking individual association addition methods
    # If appropriate, removes existing associations that must be broken to form new association
    # If nil is added, existing associations
    define_method("add_#{singular}") {|e|
      # For to_one associations, remove any existing association before adding
      send("#{name}=", nil) if !multiple
      # For from_one associations, remove argument's existing inverse association before adding
      e.send("#{inverse_name}=", nil) if !inverse_multiple && e
      # Add self to argument's inverse association
      e.send("_add_#{inverse_singular}", self)
      # Add argument to association
      self.send("_add_#{singular}", e)
    }
    # Add association link by adding argument to local collection, invoking add_hook if present
    define_method("_add_#{singular}") {|e|
      raise "Invalid type specified for #{name} association: #{e.class}" if types && !types.any?{|t| e.is_a?(t)}
      options[:add_hook].call(e, self) if options[:add_hook]
      current = send("_#{name}")
      current << e unless current.include?(e)
      e
    }
    private :"_add_#{singular}"
    
    # Remove bi-directional association links by invoking individual association removal methods
    define_method("remove_#{singular}") {|e|
      # Remove self from argument's inverse association
      e.send("_remove_#{inverse_singular}", self)
      # Remove argument from association
      self.send("_remove_#{singular}", e)
    }
    # Remove association link by removing argument from local association, invoking remove_hook if present
    define_method("_remove_#{singular}") {|e|
      options[:remove_hook].call(e, self) if options[:remove_hook]
      send("_#{name}").delete(e)
    }
    private :"_remove_#{singular}"
  end
end