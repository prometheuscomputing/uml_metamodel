# NOTE: this file must be manually required if these behaviors are desired
module UmlMetamodel
  class Classifier
    # Whether this classifier has the <<root>> or <<Root>> stereotype
    def root?
      !!(get_stereotype("root") || get_stereotype("Root"))
    end
  end
  module HasSearchableContents
    # All <<root>> classifiers within this element
    def root_classifiers(options = {})
      classifiers(options).select{|c| c.root?}
    end
  end
end