require 'cleanroom'
require_relative 'elements'
module UmlMetamodel
  
  # ----- Ruby DSL deserialization code -----
  # Deserialize UMM project from DSL string
  # parameters:
  #   - dsl - DSL string
  #   - file_path - path to display when showing stack traces for evaluated DSL
  def self.from_dsl(dsl, file_path = nil, options = {})
    UmlMetamodel::DSL.parse(dsl, file_path, options)
  end

  # Deserialize UMM project from DSL file
  def self.from_dsl_file(file_path, options = {})
    path = File.expand_path(file_path)
    self.from_dsl(File.read(path), path, options)
  end
  
  class DSL
    include Cleanroom
    attr_accessor :top_level_elements
    
    def self.parse(dsl, file_path = nil, options = {})
      file_path ||= 'UmlMetamodel::DSL'
      dsl_instance = self.new(options)
      dsl_instance.evaluate(dsl, file_path, 1)
      dsl_instance.set_deferred_reference_properties
      top_level_elements = dsl_instance.top_level_elements
      # Return singular top level element if there is only a single project in the array
      if top_level_elements.length == 1 && top_level_elements.first.is_a?(UmlMetamodel::Project)
        top_level_elements.first 
      else
        top_level_elements
      end
    end
    
    def initialize(options = {})
      # An array of the top level elements described by the DSL
      @top_level_elements = []
      # A 2-dimensional array of objects, a property, a value (or values) to dereference and apply, and
      #   optionally, a type that the reference must match
      # e.g. [<UmlMetamodel::Class Foo>, :parents, ["Bar"], UmlMetamodel::Classifier]
      @properties_to_dereference = []
      # An array representing the context stack
      @context = [@top_level_elements]
      # DSL parsing options
      #   :suppress_warnings => Wether or not to suppress warnings created during DSL parsing
      @options = options
    end
    
    # Resolve reference properties that could not be resolved earlier.
    # This method is run after all DSL entities have been processed, so it can be assumed
    # that all references should be resolvable.
    def set_deferred_reference_properties
      @properties_to_dereference.dup.each do |element, property, value, type|
        val = dereference_value(value, type)
        if !val.nil? && (!value.is_a?(Array) || val.count == value.count)
          element.send("#{property}=", val)
        else
          name = element.respond_to?(:qualified_name) ? element.qualified_name : element.name
          unless @options[:suppress_warnings]
            puts Rainbow("Warning: could not dereference: #{name}(#{element.class}) :#{property} => #{value.inspect} within #{@top_level_elements.collect{|tle|tle.qualified_name}}").magenta
          end
        end
      end
    end
    
    def project(name, opts = {}, &block)
      element(UmlMetamodel::Project, [name], nil, nil, {}, opts, &block)
    end
    expose :project
    
    def _package(type, name, opts, &block)
      references = {
        :imports => nil,
        :imported_by => nil
      }
      element(type, [name], :add_content, nil, references, opts, &block)
    end
    
    def package(name, opts = {}, &block)
      _package(UmlMetamodel::Package, name, opts, &block)
    end
    expose :package
    
    def model(name, opts = {}, &block)
      _package(UmlMetamodel::Model, name, opts, &block)
    end
    expose :model
        
    def profile(name, opts = {}, &block)
      _package(UmlMetamodel::Profile, name, opts, &block)
    end
    expose :profile
    
    # Concrete classifier types
    def klass(name, opts = {}, &block)
      classifier(UmlMetamodel::Class, name, opts, &block)
    end
    expose :klass
    
    def interface(name, opts = {}, &block)
      classifier(UmlMetamodel::Interface, name, opts, &block)
    end
    expose :interface
    
    def datatype(name, opts = {}, &block)
      classifier(UmlMetamodel::Datatype, name, opts, &block)
    end
    expose :datatype
    
    def primitive(name, opts = {}, &block)
      classifier(UmlMetamodel::Primitive, name, opts, &block)
    end
    expose :primitive
    
    def enumeration(name, opts = {}, &block)
      # Set value_type to nil to prevent automatic creation of value property, 
      # which will be described in the DSL
      classifier(UmlMetamodel::Enumeration, name, opts.merge(:value_type => nil), &block)
    end
    expose :enumeration
    
    def property(name, opts = {}, &block)
      references = {:type => UmlMetamodel::Classifier}
      element(UmlMetamodel::Property, [name], :add_property, UmlMetamodel::Classifier, references, opts, &block)
    end
    expose :property
    
    def literal(name, opts = {}, &block)
      element(UmlMetamodel::EnumerationLiteral, [name], :add_literal, UmlMetamodel::Enumeration, {}, opts, &block)
    end
    expose :literal
    
    def stereotype(name, opts = {}, &block)
      element(UmlMetamodel::Stereotype, [name], :add_content, nil, {}, opts, &block)
    end
    expose :stereotype
    
    def tag(name, opts = {}, &block)
      references = {:type => UmlMetamodel::Classifier}
      element(UmlMetamodel::Tag, [name], :add_tag, UmlMetamodel::Stereotype, references, opts, &block)
    end
    expose :tag
    
    def association(opts = {}, &block)
      references = {
        :properties => UmlMetamodel::Property,
        :association_class => UmlMetamodel::Class
      }
      element(UmlMetamodel::Association, [], :add_content, nil, references, opts.merge(:defer_setting_properties => true), &block)
    end
    expose :association
    
    def applied_stereotype(opts = {}, &block)
      references = {:instance_of => UmlMetamodel::Stereotype}
      # NOTE: applied_stereotype is not actually an element
      element(UmlMetamodel::AppliedStereotype, [], :add_applied_stereotype, UmlMetamodel::Element, references, opts, &block)
    end
    expose :applied_stereotype
    
    def applied_tag(opts = {}, &block)
      references = {:instance_of => UmlMetamodel::Tag}
      # NOTE: applied_tag is not actually an element
      element(UmlMetamodel::AppliedTag, [], :add_applied_tag, UmlMetamodel::AppliedStereotype, references, opts, &block)
    end
    expose :applied_tag
    
    private
    
    # Interpret a DSL element into a UmlMetamodel element
    def element(type, initialize_args, context_add_method, expected_context_type, references = {}, opts = {}, &block)
      # Split opts into reference options (those with values that are references) and normal options that can be set immediately
      ref_opts, normal_opts = opts.partition{|opt, _val| references.key?(opt) }.map(&:to_h)
      # Initialize the element
      element = type.new(*initialize_args, normal_opts)
      # Add the element to the parent context
      add_to_parent_context(element, context_add_method, expected_context_type)
      # Set reference options if possible. Those that can't yet be dereferenced will be stored for later dereferencing in @properties_to_dereference
      set_reference_properties(element, ref_opts, references)
      # Evaluate the passed block in the context of this element
      evaluate_context_for(element, &block) if block
    end
    
    # This method is not invoked directly, but instead by the concrete classifier type methods (class, interface, etc)
    def classifier(type, name, opts = {}, &block)
      references = {
        :parents => UmlMetamodel::Classifier,
        :children => UmlMetamodel::Classifier,
        :implements => UmlMetamodel::Interface
      }
      element(type, [name], :add_content, nil, references, opts, &block)
    end
    
    # Helper methods, not intended for direct DSL usage
    
    # Evaluate the passed block in the context of the passed element
    def evaluate_context_for(element, &block)
      @context.push element
      # Invoke Cleanroom DSL evaluation for passed block
      self.evaluate(&block)
      @context.pop
    end
    
    # Add an element to the current context
    # Prints a warning if adding a duplicate qualified_name for a non-association element
    def add_to_parent_context(element, method = nil, expected_context_type = nil)
      # Get current context
      context = @context.last
      
      # Determine add method and check for validity of context type
      if context.is_a?(Array) # @top_level_elements
        method = :<<
      else
        raise "A #{element.class} may only be defined as a top level element" unless method
        if expected_context_type && !context.is_a?(expected_context_type)
          raise "You may only define a #{element.class} within a #{expected_context_type}. Attempted to add #{element.name} to a #{@context.class}"
        end
      end
      
      # Warn if adding a duplicate qualified name
      # Currently, no warnings are printed for duplicates with associations.
      # This is because most associations do not have names and thus don't have unique qualified names
      # Additionally, this will occur for Association Class associations when the Association Class' name
      # and the Association name are the same.
      qname_context = context.qualified_name.to_s + '::' if context.respond_to?(:qualified_name)
      qname = qname_context.to_s + element.name.to_s
      # Properties don't have to have names.  Classifier#valid? tries to catch collisions between the role names of Properties (whether explicit or derived).
      duplicate = find_in_top_level_elements(qname) if element.name && !element.name.empty? && !element.is_a?(UmlMetamodel::Property)
      duplicate_with_association = element.is_a?(UmlMetamodel::Association) || duplicate.is_a?(UmlMetamodel::Association) if duplicate
      # Note: might want to limit this to just check for unnamed association properties, rather than all elements.
      #       Unnamed association roles are a lot more common than unnamed properties, classes, packages, etc.
      duplicate_with_unnamed_element = element.name.nil? || element.name.empty? if duplicate
      # TODO: This throws a warning if a classifier has two or more unnamed properties (i.e. they are association ends without specified role names).  At this point the DSL parser is not checking (and may not yet know, depending on ordering of element declarations) if the property is a member-end of an Association.  There is no reason why the properties can not be unnamed.
      if !@options[:suppress_warnings] && duplicate && !duplicate_with_association && !duplicate_with_unnamed_element
        puts "Warning: Duplicate qualified name detected: #{qname}."
        puts "  Added element:   #{qname}(#{element.class})"
        puts "  Duplicate with: #{duplicate.qualified_name}(#{duplicate.class})"
      end
      
      # Add the element to the context using the specified method
      context.send(method, element)
      element
    end
    
    def set_reference_properties(element, ref_opts, references = {})
      ref_opts.each do |property, value|
        set_reference_property(element, property, value, references[property])
      end
    end
    
    def set_reference_property(element, property, value, type = nil)
      val = dereference_value(value, type)
      if !val.nil? && (!value.is_a?(Array) || val.count == value.count)
        element.send("#{property}=", val)
      else
        @properties_to_dereference << [element, property, value, type]
      end
    end
    
    # Given a type reference or array of type references, convert them into the referenced types.
    # If type is passed, only allow references to that UML Metamodel type
    def dereference_value(value, type = nil)
      if value.is_a?(Array)
        missing_value = false
        deref_values = []
        value.each do |v|
          deref_v = dereference_value(v, type)
          unless deref_v
            missing_value = true
            break
          end
          deref_values << deref_v
        end
        missing_value ? nil : deref_values
      else
        find_in_top_level_elements(value, type)
      end
    end
    
    # Find the given qualified name in the currently parsed elements and built-in UmlMetamodel primitives
    # If type is passed, only find references to that UML Metamodel type
    def find_in_top_level_elements(qualified_name, type = nil)
      UmlMetamodel.find_in_elements(qualified_name, @top_level_elements + [UmlMetamodel.uml_package], :type => type)
    end
  end
end
