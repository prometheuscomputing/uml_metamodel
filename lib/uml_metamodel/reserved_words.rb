# TODO some of this (like the ssa stuff) should perhaps be moved into clients of UmlMetamodel.  Leaving here for now because it is convenient.
module UmlMetamodel
  
  def self.reserved_word?(word)
    reserved_words.include?(word)
  end
  
  def self.reserved_words
    @reserved_words || []
  end
  
  def self.add_reserved_words(words)
    @reserved_words ||= []
    @reserved_words += [words].flatten
    @reserved_words.uniq!
  end
  
  def self.clear_reserved_words
    @reserved_words = []
  end
  
  # Obtained by calling Sequel::Model.new.methods with sequel_change_tracker (2.16.2) and sequel_specific_associations (9.7.3) applied.
  def self.ssa_reserved_words
    %w(! != !~ <=> == === =~ [] []= __DIR__ __binding__ __id__ __send__ _insert_values _resolve_in after_association_add after_association_remove after_change_tracker_create after_change_tracker_destroy after_change_tracker_save after_commit after_create after_destroy after_destroy_commit after_destroy_rollback after_initialize after_rollback after_save after_update after_validation agree around_create around_destroy around_save around_update around_validation ask associations autoincrementing_primary_key before_association_add before_association_remove before_change_tracker_create before_change_tracker_destroy before_change_tracker_save before_create before_destroy before_save before_update before_validation cancel_action changed_columns choose class clone columns db db_schema debug define_singleton_method delete deprecated destroy display dup each enum_for eql? equal? error errors exists? extend fatal freeze frozen? gb_send_complex get_column_value hash id info inspect instance_eval instance_exec instance_of? instance_variable_defined? instance_variable_get instance_variable_set instance_variables is? is_a? is_haml? itself keys kind_of? lock! log_debug log_debug? log_deprecated? log_error log_error? log_fatal log_fatal? log_info log_info? log_warn log_warn? logger marshallable! method methods model modified! modified? new? nil? object_id object_logger or_ask parse_attribute_change pk pk_hash pretty_inspect pretty_print pretty_print_cycle pretty_print_inspect pretty_print_instance_variables primary_key private_methods protected_methods pry public_method public_methods public_send qualified_pk_hash raise_on_save_failure raise_on_save_failure= raise_on_typecast_failure raise_on_typecast_failure= refresh reload remove_instance_variable require_modification require_modification= resolve_classifier resolve_in respond_to? respond_to_chained? sanity_check save save_changes say send send_chained set set_all set_column_value set_fields set_only set_server singleton_class singleton_method singleton_method_added singleton_methods strict_param_setting strict_param_setting= taint tainted? tap this to_enum to_hash to_json to_s to_yaml trace tree_view_unique_name trust typecast_empty_string_to_nil typecast_empty_string_to_nil= typecast_on_assignment typecast_on_assignment= untaint untrust untrusted? update update_all update_fields update_only use_after_commit_rollback use_after_commit_rollback= use_transactions use_transactions= valid? validate values warn yield_self)
  end
  
  # As of Ruby 2.5.  From ruby-doc.org/core-2.5.0/doc/keywords_rdoc.html
  def self.ruby_reserved_words
    %w(__ENCODING__ __LINE__ __FILE__ BEGIN END alias and begin break case class def defined? do else elsif end ensure false for if in module next nil not or redo rescue retry return self super then true undef unless until when while yield)
  end
  
  # As of Ruby 2.5.  Obtained by calling Object.constants
  def self.ruby_object_constants
    %w(ARGF ARGV ArgumentError Array BasicObject Bignum Binding CROSS_COMPILING Class ClosedQueueError Comparable Complex ConditionVariable Data Delegator DidYouMean Dir ENV EOFError Encoding EncodingError Enumerable Enumerator Errno Exception Exception2MessageMapper FALSE FalseClass Fiber FiberError File FileTest Fixnum Float FloatDomainError FrozenError GC Gem Hash IO IOError IRB IndexError Integer Interrupt Kernel KeyError LoadError LocalJumpError Marshal MatchData Math Method Module Monitor MonitorMixin Mutex NIL NameError NilClass NoMemoryError NoMethodError NotImplementedError Numeric Object ObjectSpace Proc Process Queue RUBYGEMS_ACTIVATION_MONITOR RUBY_COPYRIGHT RUBY_DESCRIPTION RUBY_ENGINE RUBY_ENGINE_VERSION RUBY_PATCHLEVEL RUBY_PLATFORM RUBY_RELEASE_DATE RUBY_REVISION RUBY_VERSION Random Range RangeError Rational RbConfig Readline Regexp RegexpError RubyLex RubyToken RubyVM RuntimeError STDERR STDIN STDOUT ScriptError SecurityError Signal SignalException SimpleDelegator SizedQueue StandardError StopIteration String StringIO Struct Symbol SyntaxError SystemCallError SystemExit SystemStackError TOPLEVEL_BINDING TRUE Thread ThreadError ThreadGroup Time TracePoint TrueClass TypeError UnboundMethod UncaughtThrowError UnicodeNormalize Warning ZeroDivisionError)
  end
end
