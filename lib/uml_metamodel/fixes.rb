# TODO: this code belongs doesn't really belong here, and should eventually be moved.
module UmlMetamodel
  def self.fix_project_elements!(umm_project)
    fix_xsd_primitives!(umm_project)
    fix_gui_builder_profile!(umm_project)
  end
  
  # Translate references to XSD primitives to UML equivalents and remove XSD primitive package
  def self.fix_xsd_primitives!(umm_project)
    packages = umm_project.packages(:recurse => true, :include_imports => true)
    primitives_package = packages.find{|p| p.name == 'Primitives (Class based)'}
    return unless primitives_package
    # Replace all usage of XSD primitives with UML primitives
    # Also replace any inheritance from XSD primitives with UML primitive equivalent
    xsd_package = primitives_package.packages.find{|p| p.name == 'xsd'}
    xsd_primitives = xsd_package.classifiers
    xsd_primitives.each do |xsd_prim|
      properties_using_type = xsd_prim.properties_of_type.dup
      ext_children = xsd_prim.children.reject{|c| xsd_package.contents.include?(c) }
      mapped_uml_type = xsd_uml_type_mapping[xsd_prim.name]
      raise "No UML primitive mapped to XSD '#{xsd_prim.name}'" if mapped_uml_type.nil? && (properties_using_type.any? || ext_children.any?)
      # Change any properties of this type to the mapped UML primitive
      properties_using_type.each do |property|
        property.type = mapped_uml_type
      end
      # Alter any classes inheriting from this type to instead inherit from the mapped UML primitive
      ext_children.each do |child|
        xsd_prim.remove_child(child)
        mapped_uml_type.add_child(child)
      end
    end
    # Now remove all references to primitives package
    primitives_package.package = nil
    primitives_package.project = nil
    primitives_package.imported_by.dup.each do |importer|
      importer.remove_import(primitives_package)
    end
  end
  
  def self.xsd_uml_type_mapping
    {
      'string' => UmlMetamodel::PRIMITIVES[:string],
      'language' => UmlMetamodel::PRIMITIVES[:string],
      'int' => UmlMetamodel::PRIMITIVES[:integer],
      'integer' => UmlMetamodel::PRIMITIVES[:integer],
      'long' => UmlMetamodel::PRIMITIVES[:integer],
      'short' => UmlMetamodel::PRIMITIVES[:integer],
      'unsignedInt' => UmlMetamodel::PRIMITIVES[:integer],
      'unsignedLong' => UmlMetamodel::PRIMITIVES[:integer],
      'unsignedShort' => UmlMetamodel::PRIMITIVES[:integer],
      'boolean' => UmlMetamodel::PRIMITIVES[:boolean],
      'float' => UmlMetamodel::PRIMITIVES[:float],
      'decimal' => UmlMetamodel::PRIMITIVES[:number],
      'base64Binary' => UmlMetamodel::PRIMITIVES[:byte],
      'date' => UmlMetamodel::PRIMITIVES[:date],
      'dateTime' => UmlMetamodel::PRIMITIVES[:datetime],
      'time' => UmlMetamodel::PRIMITIVES[:time],
      'anyURI' => UmlMetamodel::PRIMITIVES[:uri]
    }
  end
  
  def self.fix_gui_builder_profile!(umm_project)
    packages = umm_project.packages(:recurse => true, :include_imports => true)
    gbp_package = packages.find{|p| p.name == 'Gui_Builder_Profile'}
    return unless gbp_package
    # Replace all usage of GBP primitives with UML primitives
    # Also replace any inheritance from GBP primitives with UML primitive equivalent
    gbp_primitives = gbp_package.primitives
    gbp_primitives.each do |gbp_prim|
      properties_using_type = gbp_prim.properties_of_type.dup
      ext_children = gbp_prim.children.reject{|c| gbp_package.contents.include?(c) }
      mapped_uml_type = gbp_uml_type_mapping[gbp_prim.name]
      raise "No UML primitive mapped to Gui_Builder_Profile '#{gbp_prim.name}'" if mapped_uml_type.nil? && (properties_using_type.any? || ext_children.any?)
      # Fix type for properties with GPB primitive as type
      properties_using_type.each do |property|
        property.type = mapped_uml_type
      end
      # Fix parent for Primitive classes with GPB primitive as parent
      ext_children.each do |child|
        gbp_prim.remove_child(child)
        mapped_uml_type.add_child(child)
      end
    end
    
    # Also replace usage of Gui_Builder_Profile::RichText with UML::String
    # This may need to be improved since it loses the RichText's 'markup_language' property and 'rich_text_images' association
    richtext = umm_project.find('Gui_Builder_Profile::RichText')
    if richtext
      richtext_properties = richtext.properties_of_type.dup
      richtext_properties.each do |property|
        property.type = UmlMetamodel::PRIMITIVES[:string]
      end
    end
    
    # NOTE: No longer removing GBP imports since there are non-primitives that may be referenced (e.g. RichText)
    # Now remove all references to the Gui_Builder_Profile package
    gbp_package.package = nil
    gbp_package.project = nil
    gbp_package.imported_by.dup.each do |importer|
      importer.remove_import(gbp_package)
    end
  end
  
  def self.gbp_uml_type_mapping
    {
      'Date' => UmlMetamodel::PRIMITIVES[:date],
      'Time' => UmlMetamodel::PRIMITIVES[:time],
      'Timestamp' => UmlMetamodel::PRIMITIVES[:datetime],
      'BinaryData' => UmlMetamodel::PRIMITIVES[:byte], # Is this right?
      'RegularExpression' => UmlMetamodel::PRIMITIVES[:regular_expression],
      'BigString' => UmlMetamodel::PRIMITIVES[:string]
    }
  end
end