module RubyConventions
  # This should be moved elsewhere once there is a facility for injecting it back into validation
  # In some applications this should be an error and not a warning
  def ensure_ruby_module_name(eaw)
    # puts method(:class_name).source_location
    eaw[:warnings] << "#{class_name} #{qualified_name.inspect} has a name that does not conform to convention." unless name =~ /^[A-Z]\w*/
  end
  
  def ensure_ruby_variable_name(eaw)
    return if name.nil? || name.strip.empty? # ostensibly we should get an error from other validation
    eaw[:warnings] << "#{class_name} #{qualified_name.inspect} has a name that does not conform to convention." unless name =~ /^[a-z]\w*/
  end
  
  def ensure_word_chars(eaw)
    eaw[:warnings] << "#{class_name} #{qualified_name.inspect} has a name that does not conform to convention." unless name =~ /^\w*$/
  end
end

module UmlMetamodel
  module HasValidatableContents
    # This method is meant to alter the state  of the eaw parameter (to which the caller is holding a reference).
    def validate_contained_element(ce, eaw, options)
      ce_eaw = ce.errors_and_warnings(options)
      eaw[:errors]   += ce_eaw[:errors]
      eaw[:warnings] += ce_eaw[:warnings]
    end
  end
  
  module HasSearchableContents
    include HasValidatableContents
  end
  
  class ElementBase
    def errors_and_warnings(options= {})
      empty_errors_and_warnings
    end
  
    # default implementation of validation
    def valid?(options = {})
      errors_and_warnings(options)[:errors].empty?
    end
  
    protected
    def empty_errors_and_warnings
      {:errors => [], :warnings => []}
    end
    
    def class_name
      self.class.name.split('::').last
    end
  end
  
  class Element < ElementBase
    protected
    def ensure_identifier(report)
      unless name
        report[:errors] << "#{class_name} with id: #{id ? id : 'unspecified'}, owned by #{(owner.qualified_name if owner).inspect} does not have a name."
      end
    end
    
    def check_reserved_words(report)
      if UmlMetamodel.reserved_word?(name)
        report[:warnings] << "#{class_name} #{qualified_name.inspect} uses a name that is a reserved word"
      end
    end
  end
    
  class AppliedStereotype < ElementBase
    include HasValidatableContents
    
    def errors_and_warnings(options = {})
      eaw = empty_errors_and_warnings
      eaw[:errors] << "Invalid #{class_name} -- applied to: '#{element ? element.qualified_name : 'NOTHING'}'; instance of: '#{instance_of ? instance_of.qualified_name : 'NOTHING'}'." unless name && instance_of
      applied_tags.each { |e| validate_contained_element(e, eaw, options) }
      eaw
    end
  end
  
  class AppliedTag < ElementBase
    def errors_and_warnings(options = {})
      eaw = empty_errors_and_warnings
      # not 100% that we want to require AppliedTags to have values but it does kind of make sense
      eaw[:errors] << "#{class_name}, #{qualified_name.inspect}, is invalid." unless (instance_of && value && applied_stereotype)
      eaw
    end
  end
  
  class Association < Element
    include RubyConventions
    def errors_and_warnings(options = {})
      eaw = empty_errors_and_warnings
      check_reserved_words(eaw)
      ensure_word_chars(eaw) unless name.to_s.empty?
      # It might be difficult to give informaton that will help us figure things out since Associations often have no value for name.
      named = name ? "named #{name.inspect} " : ''
      eaw[:errors] << "There is an Association #{named}that is not contained in a package.  It's properties are: #{properties.collect { |prop| prop.to_dsl }.join(';')}" unless package
      eaw[:errors] << "There is an Association #{named}in package #{package.qualified_name.inspect} that does not have two properties." unless properties.size == 2
      eaw[:errors] << "There is an Association #{named}in package #{package.qualified_name.inspect} that uses the same property for both member ends." if (properties.size == 2) && (properties[0] == properties[1])
      eaw
    end
  end
  
  class Classifier < Element
    include RubyConventions
    def errors_and_warnings(options = {})
      eaw = empty_errors_and_warnings
      check_reserved_words(eaw)
      ensure_identifier(eaw)
      ensure_ruby_module_name(eaw)
      ensure_parents(eaw)
      eaw[:errors] << "#{qualified_name} is not contained in a Package." unless package
      eaw[:errors] += check_property_rolename_uniqueness
      (applied_stereotypes + _custom_properties(options) + _custom_literals(options)).each { |e| validate_contained_element(e, eaw, options) }
      eaw
    end
    
    def ensure_parents(record)
      # TODO implement a way to make sure that all parents are part of the selected model / project.
    end
    
    # The properties all need to have unique rolenames.  Properties will either have a name already or, if part of an association, may not have a name.  If they already have a name then that will be the rolename.  If they don't already have a name then they must be part of an association and the rolename will be inferred / derived from the type of the property.  Really, we should use Appellation for this but that would introduce a dependency here that is otherwise not needed by this project.  We could do that but for now a less bulletproof implementation is used.  This implementation may fail in some situations, e.g. when the inferred rolename isn't pluralized when it should be or e.g. when the type name would need the insertion of underscores, e.g. etc. etc.
    def check_property_rolename_uniqueness
      pnames = properties.collect do |prop|
        n = prop.name.dup if prop.name # so we don't muck about with the actual name -- Don't do this if nil as JRuby can't dup NilClass -SD
        n = (prop.type.name.downcase if prop.type && prop.type.name) if n.nil? || n.empty?
        n
      end
      duplicate_names = pnames.select { |e| pnames.count(e) > 1 }.uniq
      duplicate_names.collect { |dn| "#{qualified_name} has multiple attributes with the rolename #{dn.inspect}"}
    end
    private :check_property_rolename_uniqueness
    
    # Not used by anything at the moment but might have some utility
    def valid_properties?
      errors = check_property_rolename_uniqueness
      if errors.any?
        puts errors
        false
      else
        true
      end
    end
    protected :valid_properties?
  end
  
  class EnumerationLiteral < Element
    def errors_and_warnings(options = {})
      eaw = empty_errors_and_warnings
      eaw[:errors] << "#{class_name} with name #{name} is not bound to an Enumeration." unless enumeration
      eaw
    end
  end
  
  class Enumeration < Datatype
    # This is because attributes typed as enumerations are implemented as associations.  It is the responibility of users of this model to disambiguate multiple associations from the enumeration class to the class that has multiple attributes typed as the enumeration in question.  The code below offers some suggestion on how to do this...
    def check_property_rolename_uniqueness
      pnames = properties.collect do |prop|
        n = (prop.name || '').dup
        n << (prop.opposite.name.downcase if prop.opposite && prop.opposite.name).to_s
        n
      end 
      dup_names = pnames.select { |e| pnames.count(e) > 1 }.uniq
      dup_names.collect { |dn| "#{qualified_name} has multiple attributes with the rolename #{dn.inspect}"}
    end
    private :check_property_rolename_uniqueness
  end
  
  class Package < Element
    include RubyConventions
    def errors_and_warnings(options = {})
      eaw = empty_errors_and_warnings
      ensure_identifier(eaw)
      (applied_stereotypes + _custom_contents(options)).each { |e| validate_contained_element(e, eaw, options) }
      eaw
    end
  end
  
  class Project < ElementBase
    def errors_and_warnings(options = {})
      eaw = empty_errors_and_warnings
      _custom_contents(options).each { |e| validate_contained_element(e, eaw, options) }
      eaw
    end
  end
  
  class Property < Element
    include RubyConventions
    def errors_and_warnings(options = {})
      eaw = empty_errors_and_warnings
      ensure_identifier(eaw) unless association # attributes must have identifiers
      ensure_ruby_variable_name(eaw)
      unless type
        msg = "#{class_name} #{qualified_name.inspect} owned by #{(owner.qualified_name if owner).inspect} does not have a type."
        msg_type = options[:allow_untyped_properties] ? :warnings : :errors
        eaw[msg_type] << msg
      end
      name_or_id  = (name && !name.strip.empty?) || id
      eaw[:errors] << "#{class_name}: #{qualified_name.inspect} must have a valid name or id." unless name_or_id
      vpop        = options[:validate_property_owner_proc]
      valid_owner = (owner || (vpop && vpop.call(self)))
      eaw[:errors] << "#{class_name}: #{qualified_name.inspect} does not have an owner." unless valid_owner
      eaw
    end
  end
  
  class Stereotype < Element
    include HasValidatableContents
    
    def errors_and_warnings(options = {})
      eaw = empty_errors_and_warnings
      ensure_identifier(eaw)
      eaw[:errors] << "#{qualified_name} is not contained in a Package." unless package
      tags.each { |e| validate_contained_element(e, eaw, options) }
      eaw
    end
  end
  
  class Tag < Element
    # should we require type also?
    def errors_and_warnings(options = {})
      eaw = empty_errors_and_warnings
      eaw[:errors] << "Tag of Stereotype #{stereotype.qualified_name} does not have a name." unless name
      eaw[:errors] << "#{qualified_name} is not contained in a Stereotype." unless stereotype
      eaw
    end
  end
end
