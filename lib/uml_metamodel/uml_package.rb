# Methods related to the definition of the default UML package
# The UML package is considered to be a part of all projects whether or not it is explictly included
module UmlMetamodel
  # Define UmlMetamodel::PRIMITIVES mapping for base types
  # NOTE: these primitives would be more correctly called UML Primitives and do not refer to all primitives -SD
  unless defined?(UmlMetamodel::PRIMITIVES)
    PRIMITIVES = {
      :string             => UmlMetamodel::Primitive.new("String",     :id => 'String'),
      :integer            => UmlMetamodel::Primitive.new("Integer",    :id => 'Integer'),
      :boolean            => UmlMetamodel::Primitive.new("Boolean",    :id => 'Boolean'),
      :null               => UmlMetamodel::Primitive.new("Null",       :id => 'Null'),
      :real               => UmlMetamodel::Primitive.new("Real",       :id => 'Real'),
      :byte               => UmlMetamodel::Primitive.new("ByteString", :id => 'ByteString'),
      :char               => UmlMetamodel::Primitive.new("Char",       :id => 'Char'),
      :date               => UmlMetamodel::Primitive.new("Date",       :id => 'Date'),
      :float              => UmlMetamodel::Primitive.new("Float",      :id => 'Float'),
      :unlimited_natural  => UmlMetamodel::Primitive.new("UnlimitedNatural", :id => 'UnlimitedNatural'),
      # Additional primitives not defined in OMG spec
      :datetime           => UmlMetamodel::Primitive.new("Datetime",   :id => 'Datetime'),
      :time               => UmlMetamodel::Primitive.new("Time",       :id => 'Time'),
      :uri                => UmlMetamodel::Primitive.new("Uri",        :id => 'Uri'),
      :regular_expression => UmlMetamodel::Primitive.new("RegularExpression", :id => 'RegularExpression')
    }
  end
  def self.primitives
    PRIMITIVES.values
  end
  
  def self.uml_package
    @uml_package ||= begin
      uml = UmlMetamodel::Package.new('UML', :id => 'UML')
      UmlMetamodel.primitives.each do |primitive|
        uml.add_content(primitive)
      end
      uml
    end
  end
  # Invoke uml_package to define it and assign to UML_PACKAGE constant
  unless defined?(UmlMetamodel::UML_PACKAGE)
    UML_PACKAGE = uml_package
  end
end