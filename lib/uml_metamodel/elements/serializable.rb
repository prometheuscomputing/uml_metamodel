require_relative '../dsl'
require 'fileutils'

module UmlMetamodel
  # This module provides methods for serializing and deserializing UML Metamodel objects
  module Serializable
    # It is assumed that each Serializable class defines #to_dsl individually
    
    # ----- Ruby DSL serialization code -----
    def to_dsl_file(file_path, options = {})
      # Create directory if needed
      FileUtils.mkdir_p(File.dirname(file_path))
      # Create DSL file
      File.open(file_path, 'w+') do |f|
        f.puts self.to_dsl(options)
      end
    end
    
    # ----- Ruby Marshal serialization code -----
    # Serialize this element to a string
    # Returns a string unless output_io is provided, which is used instead
    def to_marshal(output_io = nil)
      # Marshal won't accept a nil argument for io, so we must switch on output_io
      output_io ? Marshal.dump(self, output_io) : Marshal.dump(self)
    end
  
    # Serialize this element to a file specified by path
    def to_marshal_file(file_path)
      File.open(file_path, 'w+') do |f|
        self.to_marshal(f)
      end
    end
  end
  
  # ----- Ruby Marshal deserialization code -----
  # Load marshalled element or elements from a string
  # WARNING: Do not load marshal from an untrusted source as remote code execution is possible when loading.
  def self.from_marshal(marshal_string)
    Marshal.load(marshal_string)
  end

  # Load marshalled element or elements from a file specified by path
  # WARNING: Do not load marshal from an untrusted source as remote code execution is possible when loading.
  def self.from_marshal_file(file_path)
    File.open(file_path) do |f|
      self.from_marshal(f)
    end
  end
end