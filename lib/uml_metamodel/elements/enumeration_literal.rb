require_relative 'element'
module UmlMetamodel
  class EnumerationLiteral < Element
    # Define enumeration association methods: enumeration, enumeration=
    attr_assoc :enumeration, :inverse => :literals, :inverse_singular => :literal, :inverse_multiple => true
    
    def initialize(name, options = {})
      super
      self.enumeration = options[:enumeration]
    end
    
    alias_method :owner, :enumeration
    
    def qualified_name
      enumeration.qualified_name + "::" + name
    end
    
    def to_dsl(options = {})
      super
      params = []
      params << ", :id => #{id.inspect}" if id
      params << ", :documentation => #{documentation.inspect}" if documentation
      d = []
      d << "literal #{name.inspect}#{params.join}"
      dsl_stereotypes_helper(d, options)
      d.join("\n")
    end

    def to_json_obj(options = {})
      super
      j = {}
      j["name"]          = name
      j["id"]            = id if id
      j["documentation"] = documentation if documentation
      add_json_content(j, options)
      j
    end

    def self.default_contents_order
      [:applied_stereotypes]
    end
    def_delegator(self, :default_contents_order)
    
    # protected
    # def associations_for_equality
    #   super + [:enumeration]
    # end
  end
end
