require 'time'
require_relative 'element'
require_relative 'has_searchable_contents'
# A representation of a project using the UML Metamodel
module UmlMetamodel
  class Project < ElementBase
    include HasSearchableContents
    # The version of this project
    attr_accessor :version
    attr_accessor :name
    attr_accessor :description
    # The filename of the file that was used to create this project
    attr_accessor :original_filename
    # The software + version that generated this project
    attr_accessor :generated_by
    # The Time (timestamp) this project was generated
    attr_reader :generated_at
    # An array of notes for this project
    attr_accessor :notes
    # A hash storing any additional information not explicitly supported by Project
    attr_accessor :additional_info

    # Define project contents association methods: contents, contents=, add_content, remove_content
    attr_assoc :contents, :multiple => true, :singular => :content, :inverse => :project
    
    def initialize(name = nil, options = {})
      super(options)
      self.name              = name
      self.version           = options[:version]
      self.description       = options[:description]
      self.original_filename = options[:original_filename]
      self.generated_by      = options[:generated_by]
      self.generated_at      = Time.parse(options[:generated_at].to_s).utc if options[:generated_at]
      self.notes             = options[:notes] || []
      self.additional_info   = options[:additional_info] || {}
      self.contents          = options[:contents] || []
    end
    
    def self.new_from_json(options = {})
      new(options[:name], options)
    end
    
    # Custom setter for generated_at that truncates time data after seconds (usecs and nsecs are removed)
    def generated_at=(time)
      @generated_at = Time.at(time.to_i)
    end
    
    # Returns a DSL representation of this project
    # Unlike other element's #to_dsl methods, which really return DSL fragments, this method returns
    # a complete DSL, including any imported packages if they are available within the metamodel.
    def to_dsl(options = {})
      super
      options[:imports] ||=[]
      d = []
      params = []
      params << ", :version => #{version.inspect}" if version
      params << ", :description => #{description.inspect}" if description
      params << ", :original_filename => #{original_filename.inspect}" if original_filename
      params << ", :generated_by => #{generated_by.inspect}" if generated_by
      params << ", :generated_at => #{generated_at.utc.to_s.inspect}" if generated_at
      params << ", :notes => #{notes.inspect}" if notes.any?
      params << ", :additional_info => #{additional_info.inspect}" if additional_info.any?
      params << ", :id => #{id.inspect}" if id
      params << ", :documentation => #{documentation.inspect}" if documentation
      d << "project #{name.inspect}#{params.join} do"
      _custom_contents(options).each { |e| d << e.to_dsl(options).indent }
      d << 'end'
      d.join("\n")
    end
    
    def to_json_obj(options = {})
      super
      options[:imports] ||=[]
      j = {}
      j["name"]        = name if name
      j["version"]     = version if version
      j["description"] = description if description
      j["original_filename"] = original_filename if original_filename
      j["generated_by"] = generated_by if generated_by
      j["generated_at"] = generated_at.utc.to_s if generated_at
      if notes.any?
        j["notes"] = []
        notes.each { |n| j["notes"] << n }
      end
      j["additional_info"] = additional_info if additional_info.any? # FIXME how to handle this?
      j["id"] = id if id
      j["documentation"] = documentation if documentation
      add_json_content(j, options)
      j
    end
    
    def qualified_name
      nil
    end
    def inspect; "<#{self.class} #{self.name}>"; end
        
    def add_element(element)
      add_content(element)
    end

    def self.default_contents_order
      [:stereotypes, :primitives, :datatypes, :enumerations, :interfaces, :classes, :associations, :packages, :profiles, :models ]
    end
    def_delegator(self, :default_contents_order)
    
    protected
    def properties_for_equality
      super + [:version, :description, :original_filename, :generated_at, :generated_by, :notes, :additional_info]
    end
  
    def associations_for_equality
      super + [:contents]
    end
    
    private
    # Either return contents sorted in default manner or return the result of whatever method is specified in options
    def _custom_contents(options)
      options[:custom_contents] ? send(options[:custom_contents]) : contents.sort_by { |c| [c.class.to_s, c.name.to_s] }
    end
  end
end
