module UmlMetamodel
  module Packageable
    # Define package association methods: package, package=
    attr_assoc :container, :inverse => :contents, :inverse_singular => :content, :inverse_multiple => true
    def package
      container.is_a?(UmlMetamodel::Package) ? container : nil
    end
    alias_method :owner, :container
    alias_method :owner=, :container=
    alias_method :package=, :container=
    # packageable elements may also be specified as a root element in a project
    # Define project association methods: project, project=
    attr_assoc :project, :inverse => :contents, :inverse_singular => :content, :inverse_multiple => true
    attr_assoc :imported_by, :multiple => true, :inverse => :imports, :inverse_singular => :import, :inverse_multiple => true
    
    # Note -- If a package is a root package then it will report itself as its root package
    def root_package
      if package
        package.root_package
      else
        self
      end
    end
    
    # Return the project for this element if any
    # If the current element doesn't have a directly defined project, this method
    # traverses up the containment tree until a package with a project is found
    def root_project
      return project if project
      return package.root_project if package
      nil
    end
    
    def qualified_name # everything packageable is an Element and thus has a name
      qname = ''
      qname << (package.qualified_name + '::') if package
      qname << name.to_s
      qname
    end
    
    # Returns a list of containing packages in order from closest to furthest
    def containers
      package ? [package] + package.containers : []
    end
  end
end