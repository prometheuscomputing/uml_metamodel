require_relative 'element'
require_relative 'packageable'
module UmlMetamodel
  # TODO: Make Stereotype inherit from Classifier (as in OMG spec)
  #       In this case, 'tags' would be an alias for Classifier#properties
  class Stereotype < Element
    include Packageable
    attr_accessor :metaclasses
    # Define tag association methods: tags, tags=, add_tag, remove_tag
    attr_assoc :tags, :multiple => true, :singular => :tag, :inverse => :stereotype
    attr_assoc :instances, :multiple => true, :singular => :instance, :inverse => :instance_of
    
    def initialize(name, options = {})
      super
      self.tags        = options[:tags] || []
      self.instances   = options[:instances] || []
      self.metaclasses = options[:metaclasses] || []
    end
    
    def to_dsl(options = {})
      super
      d = []
      params = []
      params << (', :metaclasses => ' + metaclasses.collect{|m| m.to_s.split('::').last}.inspect) if metaclasses.any?
      params << ", :id => #{id.inspect}" if id
      params << ", :documentation => #{documentation.inspect}" if documentation
      if tags.any?
        d << "stereotype #{name.inspect}#{params.join} do"
        dsl_stereotypes_helper(d, options, :suppress_do_end)
        tags.sort_by { |e| e.name }.each do |tag|
          d << tag.to_dsl(options).indent
        end
        d << 'end'
      else
        d << "stereotype #{name.inspect}#{params.join}"
        dsl_stereotypes_helper(d, options)
      end
      d.join("\n")
    end
    
    def to_json_obj(options = {})
      super
      j = {}
      j["name"]          = name
      j["metaclasses"]   = metaclasses.collect{|m| m.to_s.split('::').last} if metaclasses.any?
      j["id"]            = id if id
      j["documentation"] = documentation if documentation
      add_json_content(j, options)
      j
    end

        
    def inspect; "<#{self.class} #{qualified_name}>"; end
    
    def add_element(element)
      case element.class.name.split('::').last
      when 'AppliedStereotype'
        add_applied_stereotype(element)
      when 'Tag'
        add_tag(element)
      else
        raise "Can you add a <#{element.class}>#{element.name} to <#{self.class}>#{name}?"
      end
    end
    
    def self.default_contents_order
      [:applied_stereotypes, :tags]
    end
    def_delegator(self, :default_contents_order)

    protected
    def properties_for_equality
      super + [:metaclasses]
    end 
  
    def associations_for_equality
      super + [:tags]
    end
  end
end
