require_relative 'element'
module UmlMetamodel
  class Tag < Element
    # Define stereotype association methods: stereotype, stereotype=
    attr_assoc :stereotype, :inverse => :tags, :inverse_singular => :tag, :inverse_multiple => true
    # Define type association methods: type, type=
    attr_assoc :type, :inverse => :tags_of_type, :inverse_singular => :tag_of_type, :inverse_multiple => true
    
    attr_assoc :instances, :multiple => true, :singular => :instance, :inverse => :instance_of
    
    def initialize(name, options = {})
      super
      self.stereotype   = options[:stereotype]
      self.instances    = options[:instances] || []
      self.type         = options[:type]
    end
    
    alias_method :owner, :stereotype
    
    def qualified_name
      raise "Unable to get qualified name for tag #{name} since it does not belong to a stereotype" unless stereotype
      stereotype.qualified_name + "::" + name
    end
    
    def to_dsl(options = {})
      params = []
      params << ", :type => #{type.qualified_name.inspect}" if type
      params << ", :id => #{id.inspect}" if id
      params << ", :documentation => #{documentation.inspect}" if documentation
      d = ["tag #{name.inspect}#{params.join}"]
      dsl_stereotypes_helper(d, options)
      d.join("\n")
    end
    
    def to_json_obj(options = {})
      j = {}
      j["name"] = name
      j["type"] = type.qualified_name if type
      j["id"]   = id if id
      j["documentation"] = documentation if documentation
      add_json_content(j, options)
      j
    end
    
    def inspect; "<#{self.class} @stereotype => #{qualified_name}>"; end
    
    def add_element(element)
      case element.class.name.split('::').last
      when 'AppliedStereotype'
        add_applied_stereotype(element)
      else
        raise "Can you add a <#{element.class}>#{element.name} to <#{self.class}>#{name}?"
      end
    end
    
    def self.default_contents_order
      [:applied_stereotypes]
    end
    def_delegator(self, :default_contents_order)
    
    protected  
    def associations_for_equality
      super + [:type] # + [:stereotype  ]
    end
  end
end
