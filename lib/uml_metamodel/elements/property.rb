require_relative 'element'
module UmlMetamodel
  class Property < Element
    attr_accessor :aggregation
    attr_accessor :visibility
    attr_accessor :is_navigable
    attr_accessor :is_unique
    attr_accessor :is_derived
    attr_accessor :is_ordered
    attr_accessor :default_value
    attr_accessor :lower, :upper
    attr_accessor :is_globally_unique
    
    # Define owner association methods: owner, owner=
    attr_assoc :owner, :inverse => :properties, :inverse_singular => :property, :inverse_multiple => true
    # Define type association methods: type, type=
    attr_assoc :type, :inverse => :properties_of_type, :inverse_singular => :property_of_type, :inverse_multiple => true
    # Define association association methods: association, association=
    attr_assoc :association, :inverse => :properties, :inverse_multiple => true, :inverse_singular => :property
    
    def initialize(name, options = {})
      super
      # Aggregation options are: :none, :shared, and :composite
      @aggregation    = options[:aggregation] || :none
      # Visibility options are :public, :protected, :private
      @visibility     = options[:visibility] || :public
      @is_navigable   = options.include?(:is_navigable) ? options[:is_navigable] : true
      # For properties with multiplicities > 1, elements are unique with respect to each other
      @is_unique      = options.include?(:is_unique) ? options[:is_unique] : true
      # Specifies whether the Property is derived, i.e., whether its value or values can be computed from other information. The default value is false.
      @is_derived     = options[:is_derived] || false
      @is_ordered     = options[:is_ordered] || false
      # The OMG UML spec is rather ambiguous on the default value here but some interpret it to be 1.  A default value of zero makes far more sense to me. -MF
      @lower          = options[:lower] || 0
      # Unbounded is represented as Infinity=1/0
      @upper          = options[:upper] || 1
      # These attributes are extensions beyond what is defined in the OMG UML specification
      # Whether this property is unique with respect to all instances of its owner (and subclasses thereof)
      @is_globally_unique = options[:is_globally_unique] || false
      @default_value = options[:default_value]
      
      self.owner = options[:owner]
      self.type  = options[:type]
      self.association = options[:association]
    end
    
    # Aggregation methods
    def composite?; aggregation == :composite; end
    def shared?   ; aggregation == :shared   ; end
    
    def private?  ; visibility == :private  ; end
    def protected?; visibility == :protected; end    
    def public?   ; visibility == :public   ; end
    
    # Attribute/association methods
    def attribute?
      !association?
    end
    def association?
      !association.nil?
    end
    
    def qualified_name
      owner.qualified_name + "::" + name.to_s
    end
    
    # Wether or not this property is navigable in one direction only
    # For non-associations, this is defined to be true
    def unidirectional?
      return true unless association
      association.properties.count{|p| p.is_navigable } == 1
    end # unidirectional
    
    # For association properties, the opposite property end
    def opposite
      if a = association
        props= a.properties
        if props.count > 2
          Kernel.warn "We aren't dealing with N-ary associations yet and you called opposite on a property that is an end in a #{props.count}-ary association"
        end
        a.properties.find { |prop| prop != self }
      else
        nil
      end
    end # opposite
    
    def multiplicity
      Range.new(lower, upper)
    end
    
    def singular?
      upper <= 1
    end
    
    def multiple?
      !singular?
    end
    
    def multiplicity_string
      m = ''
      if lower == upper
        m << lower.to_s
      elsif lower == 0 && upper == Float::INFINITY
        m << '*'
      else
        u_string = (upper == Float::INFINITY) ? '*' : upper
        m << "#{lower}..#{u_string}"
      end
      m
    end
    
    def to_dsl(options = {})
      super
      d = []
      params = []
      params << ", :type => #{(type ? type.qualified_name : nil).inspect}"
      params << ", :aggregation => :#{aggregation}" if association? && (aggregation != :none)
      params << ", :visibility => #{visibility}" unless public?
      params << ", :is_navigable => false" unless is_navigable
      params << ", :is_unique => false" unless is_unique
      params << ", :is_derived => true" if is_derived
      params << ", :is_ordered => true" if is_ordered
      # FIXME what if the default value IS an empty String??
      params << ", :default_value => #{default_value.inspect}" if default_value
      params << ", :lower => #{lower}" unless lower == 0
      params << ", :upper => #{upper == Float::INFINITY ? 'Float::INFINITY' : upper}" unless upper == 1
      params << ", :is_globally_unique => true" if is_globally_unique
      params << ", :id => #{id.inspect}" if id
      params << ", :documentation => #{documentation.inspect}" if documentation
      d << "property #{name.inspect}#{params.join}"
      dsl_stereotypes_helper(d, options)
      d.join("\n")
    end
    
    def to_json_obj(options = {})
      super
      j = {}
      j["name"]          = name
      j["id"]            = id if id
      j["type"]          = type ? type.qualified_name : nil
      j["aggregation"]   = aggregation.to_s if association? && (aggregation != :none)
      j["visibility"]    = visibility.to_s unless public?
      j["is_navigable"]  = false unless is_navigable
      j["is_unique"]     = false unless is_unique
      j["is_derived"]    = true if is_derived
      j["is_ordered"]    = true if is_ordered
      j["default_value"] = default_value.to_s if default_value
      j["lower"]         = lower.to_s unless lower == 0
      j["upper"]         = (upper == Float::INFINITY ? "infinity" : upper.to_s) unless upper == 1
      j["is_globally_unique"] = true if is_globally_unique
      j["documentation"] = documentation if documentation
      add_json_content(j, options)
      j
    end

    def inspect
      "<#{self.class} #{owner ? owner.name : ''}::#{self.name} (#{type ? type.qualified_name : '<UNTYPED>'})>"
    end
  
    def add_element(element)
      case element.class.name.split('::').last
      when 'AppliedStereotype'
        add_applied_stereotype(element)
      else
        raise "Can you add a #{element.class} to a #{self.class}?"
      end
    end
    
    def self.default_contents_order
      [:applied_stereotypes]
    end
    def_delegator(self, :default_contents_order)
      
    protected
    def properties_for_equality
      super + [:aggregation, :is_navigable, :is_unique, :is_derived, :is_ordered, :default_value, :lower, :upper, :is_globally_unique]
    end 
  
    def associations_for_equality
      super + [:type, :association] # + [:owner]
    end
    
  end
end
