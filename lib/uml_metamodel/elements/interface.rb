require_relative 'classifier'
module UmlMetamodel
  class Interface < Classifier
    # Define implementors association methods: implementors, implementors=, add_implementor, remove_implementor
    attr_assoc :implementors, :multiple => true, :singular => :implementor, :inverse => :implements, :inverse_multiple => true, :types => [UmlMetamodel::Class]
    
    def initialize(name, options = {})
      super
      @implementors = options[:implementors] || []
    end
    
    def interface?; true; end
    
    def add_element(element)
      case element.class.name.split('::').last
      when 'AppliedStereotype'
        add_applied_stereotype(element)
      when 'Property'
        add_property(element)
      else
        raise "Can you add a #{element.class} to a #{self.class}?"
      end
    end
    
    def self.default_contents_order
      [:applied_stereotypes, :properties ]
    end
    def_delegator(self, :default_contents_order)
    
    # protected
    # def associations_for_equality
    #   super + [:implementors]
    # end
  end
end
