require_relative 'element'
require_relative 'packageable'
require_relative 'container_element'
require 'rubygems' # DO NOT REMOVE! For some reason, when running JRuby within MagicDraw it is necessary. 
module UmlMetamodel
  class Classifier < Element
    include Packageable
    include ContainerElement
    
    attr_accessor :is_abstract
    
    # Define parents/children association methods: parents, parents=, add_parent, remove_parent, children, children=, add_child, remove_child
    # NOTE: Although subclasses of a classifier may only have parents and children of the same type as self, this is not enforced yet
    attr_assoc :parents, :multiple => true, :singular => :parent, :inverse => :children, :inverse_singular => :child, :inverse_multiple => true
    attr_assoc :children, :multiple => true, :singular => :child, :inverse => :parents, :inverse_singular => :parent, :inverse_multiple => true,
      :add_hook => proc{|child, parent| raise "Classifiers should not inherit from a different type - #{child.name}(#{child.class}) < #{parent.name}(#{parent.class})" unless child.is_a?(parent.class) }
    
    # Define properties association methods: properties, properties=, add_property, remove_property
    attr_assoc :properties, :multiple => true, :singular => :property, :inverse => :owner
    
    # Define properties_of_type association methods: properties_of_type, properties_of_type=, add_property_of_type, remove_property_of_type
    attr_assoc :properties_of_type, :multiple => true, :singular => :property_of_type, :inverse => :type
    
    # Define tags_of_type association methods: tags_of_type, tags_of_type=, add_tag_of_type, remove_tag_of_type
    attr_assoc :tags_of_type, :multiple => true, :singular => :tag_of_type, :inverse => :type
    
    def initialize(name, options = {})
      super
      @is_abstract            = options[:is_abstract]        || false
      self.parents            = options[:parents]            || []
      self.children           = options[:children]           || []
      self.properties         = options[:properties]         || []
      self.properties_of_type = options[:properties_of_type] || []
      self.tags_of_type       = options[:tags_of_type]       || []
      self.contents           = options[:contents]           || []
    end
    alias_method :abstract?, :is_abstract
    
    def associations
      properties.collect { |p| p.association }.compact.uniq
    end
    
    # Return all child classes recursively
    def all_children
      (children + children.collect { |c| c.all_children }.flatten).uniq
    end

    # Return all parent classes, ordered by proximity (closest first)
    def all_parents
      all_ancestors(:parents)
    end
    # Define and deprecate #ancestors alias to #all_parents
    alias_method :ancestors, :all_parents
    extend Gem::Deprecate
    deprecate :ancestors, :all_parents, 2018, 1
    
    # Return all implemented interfaces, ordered by proximity (closest first)
    def all_implements
      all_ancestors(:implements)
    end
    
    # Return all parent classes and any implemented interfaces, ordered by proximity (closest first)
    # TODO: examine the proximity ordering here more closely. 
    #       e.g. Should a property of an interface whose child interface is directly implemented by a
    #       class appear before or after a property of that class' grandparent class?
    # Type param may be:
    # nil - Returns both parents and implemented interfaces
    # :parents - Returns only parent ancestors
    # :implements - Returns only implemented interfaces
    def all_ancestors(type = nil)
      # The ordered list of all ancestors to return
      all_ancestors = []
      # The parents to use for each loop iteration
      next_parents = parents
      # The interfaces implemented to use for each loop iteration
      next_implements = ((type != :parents) && self.is_a?(UmlMetamodel::Class)) ? implements : []
      # Loop over ancestors, adding selected types to all_ancestors array
      while next_parents.any? || next_implements.any?
        all_ancestors += next_parents unless type == :implements
        all_ancestors += next_implements unless type == :parents
        # Get next iteration of next_implements by getting parents of implemented interfaces
        #   and getting interfaces implemented by parents
        if type != :parents
          next_implements = next_implements.collect { |i| i.parents }.flatten
          next_implements += next_parents.collect { |parent| parent.implements if parent.is_a?(UmlMetamodel::Class) }.compact.flatten
        end
        # Get next iteration of next_parents by getting parents of parents
        next_parents = next_parents.collect{|p| p.parents}.flatten
      end
      all_ancestors.uniq
    end

    # Return direct and inherited properties (via inheritance or an interface realization relationships)
    def all_properties
      all_classifiers = [self] + all_ancestors
      all_classifiers.collect { |classifier| classifier.properties }.flatten
    end

    def class?;     false; end
    def interface?; false; end
    def datatype?;  false; end
    def primitive?; false; end
    def enumeration?; false; end
    def association_class?; false; end
    def implements
      warn "Warning: #{self.class}#implements is deprecated!  Only UmlMetamodel::Class should call #implements.\nCalled from #{caller_locations(1,1).first}."
      []
    end
    def is_association_class?
      warn 'Warning: Classifier#is_association_class? is deprecated!  Use #association_class?'
      association_class?
    end
    
    # Does this classifier have the <<complex_attribute>> stereotype?
    # FIXME remove this.  We don't want "special" methods in here.
    def complex_attribute?
      get_stereotype("complex_attribute")
    end
    
    def to_dsl(options = {})
      super
      d = []
      params = []
      params << (', :parents => ' + parents.collect { |p| p.qualified_name }.sort.inspect) if parents.any?
      # Pretty sloppy.  Shouldn't be checking if we are a subclass from the superclass
      params << (', :implements => ' + implements.collect { |i| i.qualified_name }.sort.inspect) if self.is_a?(UmlMetamodel::Class) && implements.any?
      params << ", :id => #{id.inspect}" if id
      params << ", :documentation => #{documentation.inspect}" if documentation
      params << ", :is_abstract => true" if is_abstract
      if applied_stereotypes.any? || properties.any? || (self.is_a?(Enumeration) && literals.any?)
        d << "#{serialization_name} #{name.inspect}#{params.join} do"
        applied_stereotypes.sort_by { |as| as.qualified_name.to_s }.each do |as|
          d << as.to_dsl(options).indent
        end
        _custom_properties(options).each { |prop| d << prop.to_dsl(options).indent }
        _custom_literals(options).each { |lit| d << lit.to_dsl(options).indent } if self.is_a?(Enumeration)
        d << 'end'
      else
        d << "#{serialization_name} #{name.inspect}#{params.join}"
      end
      d.join("\n")
    end
    
    def to_json_obj(options = {})
      super
      j = {}
      j["name"]    = name
      j["parents"] = parents.collect { |p| p.qualified_name }.sort if parents.any?
      # Pretty sloppy.  Shouldn't be checking if we are a subclass from the superclass
      if self.is_a?(UmlMetamodel::Class)
        j["implements"] = implements.collect(&:qualified_name).sort if implements.any?
        j["is_singleton"] = true if is_singleton
        # FIXME not so sure about this one...
        # j["association_class_for"] = association_class_for.id if association_class_for&.id
      end
      j["id"] = id if id
      j["documentation"] = documentation if documentation
      j["is_abstract"] = true if is_abstract
      add_json_content(j, options)
      j
    end
    
    def matches?(o, recurse = false)
      super(o, recurse) && (recurse ? properties_match?(o, recurse) : true)
    end
    
    def properties_match?(o, recurse = false)
      props  = properties
      oprops = o.properties
      props.count == oprops.count &&
      props.all? { |prop| prop.matches?(oprops[props.index(prop)], recurse) }
    end
    
    def sort_properties(props, options = {})
      (options[:properties_unsorted] || options[:unsorted]) ? props : props.sort_by { |prop| (prop.name || (prop.type.name if prop.type) || prop.id) }
    end
    # Either return properties sorted in default manner or return the result of whatever method is specified in options
    def _custom_properties(options)
      options[:custom_properties] ? send(options[:custom_properties]) : sort_properties(properties)
    end
    
    protected
    def properties_for_equality
      super + [:is_abstract]
    end
  
    def associations_for_equality
      super + [:parents, :children, :properties, :contents]
    end
    
    def sort_literals(props, options = {})
      []
    end
    
    def _custom_literals(options)
      []
    end
    
  end
end
