module UmlMetamodel
  module HasSearchableContents
    # Find a qualified name relative to this element
    def find(qualified_name, options = {})
      UmlMetamodel.find(qualified_name, self, options)
    end
    
    def search_contents(options = {})
      selected_contents = contents.dup
      # Return imports according to options, if they are available (not all ContainerElement have imports)
      selected_contents += imports if options[:include_imports] && self.respond_to?(:imports)
      # FIXME: this doesn't take into account cycles, and may hang -SD
      if options[:recurse]
        children_with_contents = selected_contents.select{|c| c.is_a?(UmlMetamodel::ContainerElement) }
        child_contents = children_with_contents.collect{|c| c.search_contents(options)}.flatten
        selected_contents += child_contents
      end
      selected_contents.uniq
    end
    
    # Breakdown of content by type
    def packages(options = {})
      search_contents(options).select{|element| element.is_a?(UmlMetamodel::Package)}
    end
    
    def models(options = {})
      search_contents(options).select{|element| element.is_a?(UmlMetamodel::Model)}
    end
    
    def profiles(options = {})
      search_contents(options).select{|element| element.is_a?(UmlMetamodel::Profile)}
    end
    
    def stereotypes(options = {})
      search_contents(options).select{|element| element.is_a? UmlMetamodel::Stereotype}
    end
  
    def classifiers(options = {})
      search_contents(options).select{|element| element.kind_of? UmlMetamodel::Classifier}
    end
    
    def classes(options = {})
      search_contents(options).select{|element| element.kind_of? UmlMetamodel::Class}
    end
  
    def interfaces(options = {})
      search_contents(options).select{|element| element.kind_of? UmlMetamodel::Interface}
    end
  
    def datatypes(options = {})
      search_contents(options).select{|element| element.kind_of? UmlMetamodel::Datatype}
    end
  
    def enumerations(options = {})
      search_contents(options).select{|element| element.kind_of? UmlMetamodel::Enumeration}
    end
  
    def primitives(options = {})
      search_contents(options).select{|element| element.kind_of? UmlMetamodel::Primitive}
    end
  
    def associations(options = {})
      search_contents(options).select{|element| element.kind_of? UmlMetamodel::Association}
    end
  
    def association_classes(options = {})
      classes(options).select{|c| c.association_class_for}
    end
    
    def matches?(o, recurse = false)
      (defined?(super) ? super(o, recurse) : true) &&
      (recurse ? contents_match?(o) : true)
    end
    
    def contents_match?(o)
      c  = contents
      oc = o.contents
      c.count == oc.count &&
      c.all? { |element| element.matches?(oc[c.index(element)], true) }
    end
    
    # This method is meant to alter the state  of the eaw parameter (to which the caller is holding a reference).
    def validate_contained_element(ce, eaw, options)
      ce_eaw = ce.errors_and_warnings(options)
      eaw[:errors]   += ce_eaw[:errors]
      eaw[:warnings] += ce_eaw[:warnings]
    end
  end
end