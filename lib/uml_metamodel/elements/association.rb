require_relative 'element'
require_relative 'packageable'
module UmlMetamodel
  class Association < Element
    include Packageable
    attr_accessor :is_derived
    
    # Define association_class association methods: association_class, association_class=
    attr_assoc :association_class, :inverse => :association_class_for
    
    # Define properties association methods: properties, properties=, add_property, remove_property
    attr_assoc :properties, :multiple => true, :singular => :property, :inverse => :association
    
    # Initialize the association
    # properties - an array of the association-end properties
    def initialize(options = {})
      super(options[:name], options)
      # Specifies whether the association is derived from other model elements such as other associations or constraints. The default value is false.
      @is_derived = options[:is_derived] || false
      # Set the properties for the 'ends' of this association
      self.properties = (options[:properties] || []) unless options[:defer_setting_properties]
      self.association_class = options[:association_class]
    end
    
    def self.new_from_json(options = {})
      new(options)
    end
    
    # Alias properties= (defined by attr_assoc) as set_properties, so we can wrap it with type setting behavior
    alias_method :_set_properties, :properties=
    private :_set_properties
    # Set the properties of this association
    # NOTE: If an existing set of properties is being replaced, this does not reset the type of those properties
    def properties= new_properties
      raise "associations to more than 2 entities are unsupported" unless new_properties.length <= 2
      _set_properties(new_properties)
      if @properties.length == 2
        prop_a = new_properties[0]
        prop_b = new_properties[1]
        prop_a.type = prop_b.owner
        prop_b.type = prop_a.owner
      end
    end
    
    def to_dsl(options = {})
      super
      params = []
      params << ", :name => #{name.inspect}" if name && !name.empty?
      params << ", #{assocaition_class_string}" if association_class
      params << ", :id => #{id.inspect}" if id
      params << ", :documentation => #{documentation.inspect}" if documentation
      d = ["association #{properties_string}#{params.join}"]
      dsl_stereotypes_helper(d, options)
      d.join("\n")
    end
    
    def to_json_obj(options = {})
      super
      j = {}
      j["name"] = name if name && !name.empty?
      j["association_class"] = association_class.qualified_name if association_class
      j["id"] = id if id
      j["properties"] = properties.collect { |prop| prop.name ? prop.qualified_name : prop.id }
      j["documentation"] = documentation if documentation
      add_json_content(j, options)
      j
    end
    
    def inspect
      ret = "<#{self.class}"
      ret << " #{name}" if name && !name.empty?
      ret << ", #{properties_string}"
      ret << ", #{assocaition_class_string}" if association_class
      ret << '>'
      ret
    end
    
    def properties_string
      ':properties => ' + properties.collect { |prop| prop.name ? prop.qualified_name : prop.id }.inspect
    end
    
    def assocaition_class_string
      ":association_class => #{association_class.qualified_name.inspect}"
    end
    
    def add_element(element)
      case element.class.name.split('::').last
      when 'AppliedStereotype'
        add_applied_stereotype(element)
      else
        raise "Can you add a #{element.class} to a #{self.class}?"
      end
    end
    
    def self.default_contents_order
      [:applied_stereotypes]
    end
    def_delegator(self, :default_contents_order)
    
    protected
    def properties_for_equality
      super + [:is_derived]
    end
    
    def associations_for_equality
      super + [:association_class, :properties]
    end
    
  end
end
