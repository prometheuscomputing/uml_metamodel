require_relative 'element_base'
module UmlMetamodel
  class AppliedTag < ElementBase
    # The value of the tag being applied
    attr_accessor :value
    # The AppliedStereotype that this AppliedTag belongs to
    # Define stereotype association methods: applied_stereotype, applied_stereotype=
    attr_assoc :applied_stereotype, :inverse => :applied_tags, :inverse_singular => :applied_tag, :inverse_multiple => true
    
    attr_assoc :instance_of, :inverse => :instances, :inverse_singular => :instance, :inverse_multiple => true
    
    def initialize(options = {})
      super
      self.instance_of        = options[:instance_of]
      self.applied_stereotype = options[:applied_stereotype]
      self.value              = options[:value]
    end
    
    def self.new_from_json(options = {})
      new(options)
    end
    
    def name
      instance_of.name if instance_of
    end
    
    def qualified_name
      qn = ''
      qn << "#{instance_of.qualified_name}::" if instance_of
      qn << value
      qn
    end
    
    def to_dsl(options = {})
      super
      params = []
      params << ", :id => #{id.inspect}" if id
      params << ", :documentation => #{documentation.inspect}" if documentation
      "applied_tag :instance_of => #{instance_of.qualified_name.inspect}, :value => #{value.inspect}#{params.join}"
    end
    
    def to_json_obj(options = {})
      super
      j = {}
      j["instance_of"]   = instance_of.qualified_name
      j["value"]         = value # should this be to_s???
      j["id"]            = id if id
      j["documentation"] = documentation if documentation
      j
    end
    
    def inspect; "<#{self.class} #{self.qualified_name}>"; end
    
    def_delegator(self, :default_contents_order)
    
    protected
    def properties_for_equality; super + [:value]; end
    def associations_for_equality; super + [:instance_of]; end
  end
end
