require_relative 'classifier'
module UmlMetamodel
  class Datatype < Classifier
    def datatype?; true; end
    
    def add_element(element)
      case element.class.name.split('::').last
      when 'AppliedStereotype'
        add_applied_stereotype(element)
      when 'Property'
        add_property(element)
      else
        raise "Can you add a #{element.class} to a #{self.class}?"
      end
    end
    
    def self.default_contents_order
      [:applied_stereotypes, :properties ]
    end
    def_delegator(self, :default_contents_order)
  end
end
