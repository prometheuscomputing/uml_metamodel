require_relative 'element'
require_relative 'packageable'
require_relative 'container_element'
module UmlMetamodel
  class Package < Element
    include Packageable
    include ContainerElement
    # Define import association methods: imports, imports=, add_import, remove_import, imported_by, imported_by=, add_imported_by, remove_imported_by
    # NOTE: It is expected that imports are either included either in the project contents or imports
    #       The add_hook defined below automatically adds the imported package to the importer's project
    #       to maintain bacwards compatibility with code that doesn't do this manually
    #       It is likely that this behavior will be deprecated in future, so don't rely on it.
    attr_assoc :imports, :multiple => true, :singular => :import, :inverse => :imported_by, :inverse_multiple => true,
    # FIXME this add_hook may be a bad idea.  We eventually want to support shared packages, i.e. shared between different projects.  Projects aggregate elements rather than being composed of elements.  Perhaps Packageables should be able to be in multiple Projects.
      :add_hook => proc { |import, importer|
        # Set the imported package's project to the importer's project if it is not already defined
        import.root_package.project = importer.root_project if importer.root_project && !import.root_project
      }
    attr_assoc :imported_by, :multiple => true, :inverse => :imports, :inverse_singular => :import, :inverse_multiple => true
    
    attr_accessor :version
    
    def initialize(name, options = {})
      super
      self.version  = options[:version]
      self.contents = options[:contents] || []
      self.imports  = options[:imports] || []
      self.imported_by = options[:imported_by] || []
    end
    
    # Represent this package as a DSL string
    # options:
    #   :import - Whether or not this package is an import only, and not otherwise a part of the containment tree.
    #   :imports - If an imports array is passed, any imports (of this package or any contained elements) will be added to it
    def to_dsl(options = {})
      super
      d = []
      options[:imports].push(*imports) if options[:imports]
      params = []
      params << (', :imports => ' + imports.sort_by{|i|i.name}.collect{|i|i.qualified_name}.inspect) if imports.any?
      params << ", :id => #{id.inspect}" if id
      params << ", :version => #{version.inspect}" if version
      params << ", :documentation => #{documentation.inspect}" if documentation
      d << "#{serialization_name} #{name.inspect}#{params.join} do"
      applied_stereotypes.sort_by{|as| as.qualified_name.to_s }.each do |as|
        d << as.to_dsl(options).indent
      end
      _custom_contents(options).each { |e| d << e.to_dsl(options).indent }
      d << 'end'
      d.join("\n")
    end
    
    def to_json_obj(options = {})
      super
      j = {}
      j["name"]          = name
      options[:imports].push(*imports) if options[:imports]
      j["imports"]       = imports.sort_by { |i| i.name }.collect { |i| i.qualified_name } if imports.any?
      j["id"]            = id if id
      j["version"]       = version if version
      j["documentation"] = documentation if documentation
      add_json_content(j, options)
      j
    end
    
    def add_element(element)
      case element.class.name.split('::').last
      when 'AppliedStereotype'
        add_applied_stereotype(element)
      else
        add_content(element)
      end
    end
    
    def self.default_contents_order
      [:applied_stereotypes, :stereotypes, :primitives, :datatypes, :enumerations, :interfaces, :classes, :associations, :packages, :profiles, :models ]
    end
    def_delegator(self, :default_contents_order)
    
    protected
    def properties_for_equality
      super + [:version]
    end
  
    def associations_for_equality
      super + [:imports, :contents]
    end
    
    private
    def sort_elements(elements, options = {})
      elements.sort_by do |e|
        criteria = [e.class.to_s, e.name.to_s]
        # Add property names to sort criteria for associations
        [0,1].each do |i|
          if e.is_a?(UmlMetamodel::Association) && e.properties[i]
            criteria << e.properties[i].qualified_name
          else
            criteria << ''
          end
        end
        criteria << e.id
        criteria
      end
    end
    # Either return contents sorted in default manner or return the result of whatever method is specified in options
    def _custom_contents(options)
      if options[:custom_contents]
        send(options[:custom_contents])
      else
        sort_elements(contents, options)
      end
    end
  end # class Package
end
