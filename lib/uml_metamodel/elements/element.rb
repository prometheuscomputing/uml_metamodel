require_relative 'element_base'
module UmlMetamodel
  class Element < ElementBase
    attr_accessor :name
    # Define applied stereotypes association methods: :applied_stereotypes, :applied_stereotypes=, :add_applied_stereotype, :remove_applied_stereotype
    attr_assoc :applied_stereotypes, :multiple => true, :singular => :applied_stereotype, :inverse => :element
    
    def initialize(name, options = {})
      super(options)
      self.name = name
      self.applied_stereotypes = options[:applied_stereotypes] || []
    end
    
    def self.new_from_json(options = {})
      new(options[:name], options)
    end
      
    def apply_stereotype stype
      # first check if this stereotype is already applied
      found = applied_stereotypes.find{|as| as.name == stype.name}
      return found if found
      # Not found, so create a new applied stereotype and add it to this element
      UmlMetamodel::AppliedStereotype.new(:instance_of => stype, :element => self)
    end
    
    def get_stereotype stereotype_name
      applied_stereotypes.find{|as| as.name == stereotype_name}
    end
    
    def get_tag_value stereotype_name, tag_name
      if applied_stereotype = get_stereotype(stereotype_name)
        if applied_tag = applied_stereotype.applied_tags.find{|t| t.name == tag_name}
          applied_tag.value
        end
      end
    end
    
    def set_tag_value stereotype_name, tag_name, value
      applied_stereotype = get_stereotype(stereotype_name)
      raise "No stereotype named #{stereotype_name} is applied to #{self.class} #{self.name}" unless applied_stereotype
      # Check for an existing applied_tag
      applied_tag = applied_stereotype.applied_tags.find{|t| t.name == tag_name}
      # If not found, create a new one
      unless applied_tag
        tag = applied_stereotype.instance_of.tags.find{|t| t.name == tag_name}
        raise "No tag named #{tag_name} is defined on stereotype #{stereotype_name}" unless tag
        applied_tag = UmlMetamodel::AppliedTag.new(:instance_of => tag, :applied_stereotype => applied_stereotype)
      end
      applied_tag.value = value
    end
    
    def matches?(o, recurse = false)
      super(o, recurse) && (recurse ? applied_stereotypes_match?(o, recurse) : true)
    end
    
    def applied_stereotypes_match?(o, recurse = false)
      asts  = applied_stereotypes
      oasts = o.applied_stereotypes
      asts.all? { |ast| ast.matches?(oasts[asts.index(ast)], recurse) }
    end
    
    def inspect; "<#{self.class} #{self.name}>"; end
    
    protected
    def associations_for_equality
      super + [:applied_stereotypes]
    end
    
    def dsl_stereotypes_helper(dsl_array, options, suppress_do_end = false)
      if applied_stereotypes.any?
        dsl_array.last << ' do' unless suppress_do_end
        applied_stereotypes.sort_by { |as| as.qualified_name.to_s }.each do |as|
          dsl_array << as.to_dsl(options).indent
        end
        dsl_array << 'end' unless suppress_do_end
      end
    end
  end
end
