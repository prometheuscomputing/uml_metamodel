require_relative 'element_base'
module UmlMetamodel
  class AppliedStereotype < ElementBase
    # The instance of element to which a given stereotype is applied
    # Define element association methods: element, element=
    attr_assoc :element, :inverse => :applied_stereotypes, :inverse_singular => :applied_stereotype, :inverse_multiple => true

    # Define applied_tag association methods: applied_tags, applied_tags=, add_applied_tag, remove_applied_tag
    attr_assoc :applied_tags, :multiple => true, :singular => :applied_tag, :inverse => :applied_stereotype
    
    attr_assoc :instance_of, :inverse => :instances, :inverse_singular => :instance, :inverse_multiple => true
    
    def initialize(options = {})
      super
      self.instance_of  = options[:instance_of]
      self.element      = options[:element]
      self.applied_tags = options[:applied_tags] || []
    end
    
    def self.new_from_json(options = {})
      new(options)
    end
    
    def matches?(o, recurse = false)
      super(o, recurse) && (recurse ? applied_tags_match?(o) : true)
    end
    
    def applied_tags_match?(o)
      ats  = applied_tags
      oats = o.applied_tags
      ats.all? { |at| at.matches?(oats[ats.index(at)], true) }
    end
    
    def name
      instance_of.name if instance_of
    end
    
    def qualified_name
      instance_of.qualified_name if instance_of
    end
    
    def to_dsl(options = {})
      super

      d = []
      params = []
      params << ", :id => #{id.inspect}" if id
      params << ", :documentation => #{documentation.inspect}" if documentation
      if applied_tags.any?
        d << "applied_stereotype :instance_of => #{qualified_name.inspect}#{params.join} do"
        applied_tags.each do |applied_tag|
          d << applied_tag.to_dsl(options).indent
        end
        d << 'end'
      else
        d << "applied_stereotype :instance_of => #{qualified_name.inspect}#{params.join}"
      end
      d.join("\n")
    end
    
    def to_json_obj(options = {})
      super
      j = {}
      j["instance_of"]   = instance_of.qualified_name
      j["id"]            = id if id
      j["documentation"] = documentation if documentation
      add_json_content(j, options)
      j
    end
    
    def inspect; "<#{self.class} @stereotype => #{qualified_name}>"; end
    
    def self.default_contents_order
      [:applied_tags]
    end
    def_delegator(self, :default_contents_order)
    
    protected
    def associations_for_equality; super + [:instance_of, :element, :applied_tags]; end
  end
end
