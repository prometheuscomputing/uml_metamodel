require_relative 'has_searchable_contents'
module UmlMetamodel
  module ContainerElement
    include HasSearchableContents
    # Define package contents association methods: contents, contents=, add_content, remove_content
    attr_assoc :contents, :multiple => true, :singular => :content, :inverse => :container
  end
end