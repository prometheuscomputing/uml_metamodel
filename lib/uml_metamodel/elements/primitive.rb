require_relative 'datatype'
module UmlMetamodel
  class Primitive < Datatype
    def primitive?; true; end
    
    # The primitives described by primitives.rb are not typically included in the serialization of a model expect for where they are used as the types of Properties.  It is sometimes important to know if a classifier has been encountered before while processing a model.  In the case of a deserialized model it is likely that classifiers will be compared to the primitives loaded from primitives.rb in order to determine whether that classifier is one of those primitives.  The deserialized classifiers will not be the same objects as those loaded from primitives.rb yet we need to treat them as such.
    def eql? other
      return false unless other.is_a?(UmlMetamodel::Primitive)
      self.qualified_name == other.qualified_name
    end
    def == other
      eql?(other)
    end
    def hash
      self.qualified_name.hash
    end
    
    def base_primitive
      return self if UmlMetamodel.primitives.include?(self)
      (all_parents & UmlMetamodel.primitives).first
    end
    
    def add_element(element)
      case element.class.name.split('::').last
      when 'AppliedStereotype'
        add_applied_stereotype(element)
      when 'Property'
        add_property(element) # FIXME this is an association
        puts Rainbow("Datatypes can not have associations! You can't add #{element.name} to <#{self.class}>#{name}").red
      else
        raise "Can you add a <#{element.class}>#{element.name} to <#{self.class}>#{name}?"
      end
    end
    
    def default_classifier_order
      ["applied_stereotypes"]
    end
    
  end
end