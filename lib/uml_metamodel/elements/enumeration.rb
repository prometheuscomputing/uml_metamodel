require_relative 'datatype'
module UmlMetamodel
  class Enumeration < Datatype
    # Define literals association methods: literals, literals=, add_literal, remove_literal
    attr_assoc :literals, :multiple => true, :singular => :literal, :inverse => :enumeration
    
    def enumeration?; true; end
    def initialize(name, options = {})
      super(name, options)
      self.literals = options[:literals] || []
      # Default value property to string type
      value_type = UmlMetamodel::PRIMITIVES[:string]
      # Set value_type according to options if specified. Nil type indicates that value property should not be added.
      value_type = options[:value_type] if options.key?(:value_type)
      add_value_property(value_type) if value_type && !value_property_already_defined?
    end
    
    def value_property_already_defined?
      properties.find { |prop| prop.name == 'value' } || parents.any? { |parent| parent.value_property_already_defined }
    end
    
    def add_literal_name(literal_name)
      literal = EnumerationLiteral.new(literal_name)
      add_literal(literal)
    end
    
    def all_literals
      all_enumerations = [self] + all_children
      all_literals = []
      all_enumerations.each do |enumeration|
        enumeration.literals.each do |literal|
          all_literals << literal unless all_literals.any?{|l| l.name == literal.name}
        end
      end
      all_literals
    end

    def add_value_property(value_type)
      value_property = UmlMetamodel::Property.new("value")
      value_property.type = value_type
      add_property value_property
    end
    
    def add_element(element)
      case element.class.name.split('::').last
      when 'AppliedStereotype'
        add_applied_stereotype(element)
      when 'Property'
        add_property(element)
      when 'EnumerationLiteral'
        add_literal(element)
      else
        raise "Can you add a #{element.class} to a #{self.class}?"
      end
    end
        
    def self.default_contents_order
      [:applied_stereotypes, :properties, :literals]
    end
    def_delegator(self, :default_contents_order)

    protected
    def associations_for_equality
      super + [:literals]
    end
    
    def _custom_literals(options)
      options[:custom_literals] ? send(options[:custom_literals]) : sort_literals(literals)
    end
    
    def sort_literals(props, options = {})
      (options[:literals_unsorted] || options[:unsorted]) ? literals : literals.sort_by(&:name)
    end
  end
end
