require 'securerandom'
require_relative 'serializable'
module UmlMetamodel
  # TODO registry for IDs.  validate uniq IDs. note that there may be times when we need two elements with the same ID because we are comparing versions of a model or merging one into another, etc.  Keep this in mind and do not have a situation where id is checked against all known ids and invalidated if it already exists when it is set.  We need something a little smarter than that.
  class ElementBase
    extend Forwardable
    include Serializable
    # Abstract class
    attr_accessor :documentation
    # Unique identifier for this element
    attr_reader :id
    
    def initialize(options = {})
      @documentation = options[:documentation]
      @id            = options[:id].to_s if options[:id]
    end
    
    def id=(value)
      @id = value.to_s
    end
    
    def to_dsl(options = {})
      raise_on_error(options)
    end
    
    def to_json_obj(options = {})
      raise_on_error(options)
    end
    
    def raise_on_error(options = {})
      return if options[:no_error_checking]
      eaw = errors_and_warnings(options)
      unless eaw[:errors].empty?
        raise "#{inspect}(#{qualified_name}) #{"with id: #{id} " if id}is invalid.\nErrors:\n#{eaw[:errors].collect { |e| '    ' + e }.join("\n")}"
      end
    end
    
    def inspect; "<#{self.class} #{self.name}>"; end
    
    def ensure_id
      self.id ||= SecureRandom.uuid
    end
    
    def matches?(o, _recurse = false)
      # puts "Does #{qualified_name} match?"#" #{o.inspect}?"#" -- #{self.class.ancestors.inspect}"
      (o.class == self.class) && 
      properties_for_equality.all? do |getter|
        answer = send(getter) == o.send(getter)
        puts "#{qualified_name} did not match because " unless answer
        answer
      end &&
      associations_for_equality.all? do |getter|
        answer = send("_"+getter.to_s).collect(&:id) == o.send("_"+getter.to_s).collect(&:id)
        puts "#{qualified_name} did not match" unless answer
        answer
      end
    end
    
    def self.default_contents_order; []; end
    
    def add_element(element)
      raise "#add_element is not supported for #{self.class}"
    end

    protected
    # A list of the properties compared for equality checks
    def properties_for_equality; [:name, :id, :documentation]; end
    # A list of the associations compared for equality checks
    def associations_for_equality; []; end
    
    def self.serialization_name
      @serialization_name ||= name.to_s.split('::').last.downcase
    end
    
    def serialization_name
      self.class.serialization_name
    end

  end
end
