require_relative 'classifier'
module UmlMetamodel
  class Class < Classifier
    attr_accessor :is_singleton
    
    # Define association_class_for association methods: association_class_for, association_class_for=
    attr_assoc :association_class_for, :inverse => :association_class
    
    # Define implements association methods: implements, implements=, add_implements, remove_implements
    attr_assoc :implements, :multiple => true, :inverse => :implementors, :inverse_singular => :implementor, :inverse_multiple => true
    
    def class?; true; end
    
    def association_class?
      !!association_class_for
    end
    
    def add_element(element)
      case element.class.name.split('::').last
      when 'AppliedStereotype'
        add_applied_stereotype(element)
      when 'Property'
        add_property(element)
      else
        add_content(element)
      end
    end
    
    def self.default_contents_order
      [:applied_stereotypes, :classes, :properties ]
    end
    def_delegator(self, :default_contents_order)
    
    def self.serialization_name
      "klass"
    end
    
    protected
    def properties_for_equality
      super + [:is_singleton]
    end
  
    def associations_for_equality
      super + [:association_class_for, :implements]
    end
  end
end
